var count = 0;
/**
 * @module Feature
 */
define([], function () {
    /**
     * Abstract class that represents a collection of tiles with a location
     * @alias module:Feature
     * @constructor
     */
    var Feature = function (location) {
        this.id = count++;
        this.location = location;
    };
    return Feature;
});
