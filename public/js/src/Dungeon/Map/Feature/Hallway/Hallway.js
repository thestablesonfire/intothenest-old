/**
 * @module Hallway
 */
define([
    'Feature',
    'Pathfinder',
    'TileFactory'
], function (Feature, Pathfinder, TileFactory) {
    /**
     * @alias module:Hallway
     * @param start
     * @param end
     * @param map
     * @constructor
     */
    var Hallway = function (start, end, map) {
        this.start = start;
        this.end = end;
        this.map = map;
        this.tiles = this.getNullMap();
        this.tileFactory = new TileFactory();


        Feature.call(this, {y:0, x:0});
        this.generatePathway();
    };

    Hallway.prototype = Object.create(Feature.prototype);

    /**
     * Finds a path between this.start and this.end and sets this hallway's tiles to that path
     */
    Hallway.prototype.generatePathway = function () {
        var path = new Pathfinder(this.start, this.end, this.map).findPath();
        this.setTiles(path);
    };

    /**
     * Creates a blank 2d array the size of the passed map
     * @returns {Array}
     */
    Hallway.prototype.getNullMap = function () {
        var tiles = [];
        for (var i = 0; i < this.map.length; i++) {
            tiles[i] = [];
        }
        return tiles;
    };

    // Hallway.prototype.printHallway = function () {
    //     var row;
    //     for (var i = 0; i < this.map.length; i++) {
    //         row = '';
    //         for (var j = 0; j < this.map[0].length; j++) {
    //             if (this.tiles[i][j]) {
    //                 row += this.tiles[i][j].getSymbol() + ' ';
    //             }
    //             else {
    //                 row += '  ';
    //             }
    //         }
    //         console.log(row);
    //     }
    //     console.log('');
    // };

    /**
     * Sets the map spaces to Hallway tiles if in the path
     * @param path
     */
    Hallway.prototype.setTiles = function (path) {
        for (var pathTile in path) {
            var tile = path[pathTile];
            var location = tile.location;
            this.tiles[location.y][location.x] = this.tileFactory.getHallwayTile(location.y, location.x);
        }
    };

    return Hallway;
});
