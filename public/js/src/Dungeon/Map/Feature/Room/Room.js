/**
 * @module Room
 */

define([
    'Door',
    'Feature',
    'Staircase',
    'TileFactory',
    'TileTypes',
    'Utilities'
], function (
    Door,
    Feature,
    Staircase,
    TileFactory,
    TileTypes,
    Utilities
) {
    /**
     * A very basic square room
     * @alias module:Room
     * @param location
     * @constructor
     */
    var Room = function (location) {
        "use strict";

        /**
         * Location (top-left corner) of this Room
         */
        this.location = location;

        /**
         * Room height
         * @type {number}
         */
        this.height = 0;

        /**
         * Room width
         * @type {number}
         */
        this.width = 0;

        /**
         * Holds the staircases (if any) in the room
         * @type {Array}
         */
        this.staircases = [];

        /**
         * The local tiles of the room
         * @type {Array}
         */
        this.tiles = [];

        /**
         * All the doors in the room
         * @type {Array}
         */
        this.doors = [];

        /**
         * Holds the chests (if any) in the room
         * @type {Array}
         */
        this.chests = [];

        /**
         * The probability that a chest will spawn in this room
         * @type {number}
         */
        this.chestChance = .5;

        /**
         * Creates Tiles
         */
        this.tileFactory = new TileFactory();

        Feature.call(this, location);

        this.init();
    };

    Room.prototype = Object.create(Feature.prototype);

    /**
     * Max and Min dimensions a room can have
     * @type {number}
     */
    var minHeight = 6;
    var minWidth = 6;
    var maxHeight = 10;
    var maxWidth = 10;

    /**
     * Max and Min number of doors a room can have
     * @type {number}
     */
    var minDoors = 1;
    var maxDoors = 3;

    /**
     * Initializes the room dimensions and builds its tiles
     */
    Room.prototype.init = function () {
        this.setDimensions();
        this.buildTiles();
        this.setDoors();
        this.placeChests();
        this.globalizeRoom();
    };

    /**
     * Sets the maximum height and width of the room
     */
    Room.prototype.setDimensions = function () {
        this.setHeight();
        this.setWidth();
    };

    /**
     * Sets the room height to be a random number between minHeight and maxHeight
     */
    Room.prototype.setHeight = function () {
        this.height = Utilities.generateRandomNumber(minHeight, maxHeight);
    };

    /**
     * Sets the room width to be a random number between minWidth and maxWidth
     */
    Room.prototype.setWidth = function () {
        this.width = Utilities.generateRandomNumber(minWidth, maxWidth);
    };

    /**
     * Builds the tiles for the room
     */
    Room.prototype.buildTiles = function () {
        for (var i = 0; i < this.height; i++) {
            this.tiles[i] = [];
            for (var j = 0; j < this.width; j++) {
                if (this.isOuterPerimeter(i,j)) {
                    this.tiles[i][j] = this.tileFactory.getBlankTile(i,j);
                }
                else if (this.isInnerPerimeter(i,j)) {
                    this.tiles[i][j] = this.tileFactory.getWallTile(i, j);
                } else {
                    this.tiles[i][j] = this.tileFactory.getFloorTile(i,j);
                }
            }
        }
    };

    /**
     * Converts all tile locations in this room from local room coordinates to global map coordinates
     */
    Room.prototype.globalizeRoom = function () {
        for (var i = 0; i < this.height; i++) {
            for (var  j = 0; j < this.width; j++) {
                var thisTile = this.tiles[i][j];
                var location = thisTile.location;
                thisTile.location = this.globalizeLocation(location.y, location.x);
            }
        }
    };

    /**
     * Converts all tile locations in this room from global map coordinates to local room coordinates
     */
    Room.prototype.deGlobalizeRoom = function () {
        for (var i = 0; i < this.height; i++) {
            for (var  j = 0; j < this.width; j++) {
                var thisTile = this.tiles[i][j];
                var location = thisTile.location;
                thisTile.location = this.deGlobalizeLocation(location);
            }
        }
    };

    /**
     * Converts local tile location to global map tile location
     * @param i
     * @param j
     * @returns {{x: number, y: number}}
     */
    Room.prototype.globalizeLocation = function (i, j) {
        return {
            x: j + this.location.x,
            y: i + this.location.y
        }
    };

    /**
     * Converts global tile location to local room tile location
     * @param tileLocation
     * @returns {{x: number, y: number}}
     */
    Room.prototype.deGlobalizeLocation = function (tileLocation) {
        return {
            x: tileLocation.x - this.location.x,
            y: tileLocation.y - this.location.y
        };
    };

    /**
     * Returns true if the tile is part of the outer perimeter of the room and should be left blank
     * @param y
     * @param x
     * @returns {boolean}
     */
    Room.prototype.isOuterPerimeter = function (y, x) {
        return y === 0 || x === 0 || y === this.height - 1 || x === this.width-1;
    };

    /**
     * Returns true if the location passed is part of the inner perimeter of the room and should be a wall
     * @param y
     * @param x
     * @returns {boolean}
     */
    Room.prototype.isInnerPerimeter = function (y, x) {
        var notOutsideX = (x > 0 && x < this.width - 1);
        var notOutsideY = (y > 0 && y < this.height - 1);

        var firstRow = (y === 1 && notOutsideX);
        var lastRow = (y === this.height - 2 && notOutsideX);
        var firstCol = (x === 1 && notOutsideY);
        var lastCol = (x === this.width - 2 && notOutsideY);

        return firstRow || lastRow || firstCol || lastCol;
    };

    /**
     * Picks a random number of doors and places them in the walls
     */
    Room.prototype.setDoors = function () {
        var numDoors = Utilities.generateRandomNumber(minDoors, maxDoors);
        var edgeTiles = this.gatherWallTiles();
        var index;

        for (var i = 0; i < numDoors; i++) {
            index = Utilities.generateRandomNumber(0, edgeTiles.length-1);
            var thisTile = edgeTiles[index];
            if (index < edgeTiles.length && !this.isCornerTile(thisTile) && !this.isDoorAdjacent(thisTile)) {
                var door = this.tileFactory.getDoorTile(thisTile.location.y, thisTile.location.x);
                this.replaceRoomTileLocal(thisTile, door);
                this.doors.push(door);
                edgeTiles.splice(index, 1);
            } else {
                i--;
            }
        }
    };

    /**
     * Collects all edge tiles in a room in an array
     * @returns {Array}
     */
    Room.prototype.gatherWallTiles = function () {
        var edgeTiles = [];
        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                if (this.isInnerPerimeter(i,j)) {
                    edgeTiles.push(this.tiles[i][j]);
                }
            }
        }
        return edgeTiles;
    };

    /**
     * Returns true if tile is in the corner of the room
     * @param tile
     * @returns {boolean}
     */
    Room.prototype.isCornerTile = function (tile) {
        var location = tile.location;
        var x = location.x;
        var y = location.y;
        var maxX = this.tiles[0].length-2;
        var maxY = this.tiles.length-2;

        var topLeftCorner = (x === 1 && y === 1);
        var topRightCorner = (x === maxX && y === 1);
        var bottomLeftCorner = (x === 1 && y === maxY);
        var bottomRightCorner = (x === maxX && y === maxY);

        return topLeftCorner || topRightCorner || bottomLeftCorner || bottomRightCorner;
    };

    /**
     * Returns true if tile is adjacent to a door
     * @param tile
     * @returns {boolean}
     */
    Room.prototype.isDoorAdjacent = function (tile) {
        var adjacentLocations = tile.getAdjacentTiles();
        var thisLocation;
        var thisTile;

        for (var location in adjacentLocations) {
            thisLocation = adjacentLocations[location];
            if (this.tileExistsAtLocation(thisLocation)) {
                thisTile = this.tiles[thisLocation.y][thisLocation.x];
                if (thisTile && thisTile.getType() === TileTypes.DoorClosed) {
                    return true;
                }
            }
        }
        return false;
    };

    /**
     * Returns whether a tile exists in the passed location
     * @param location
     * @returns {boolean}
     */
    Room.prototype.tileExistsAtLocation = function (location) {
        return (typeof this.tiles[location.y] !== 'undefined' && typeof this.tiles[location.y][location.x] !== 'undefined');
    };

    /**
     * Replaces oldTile with newTile when oldTile has a local(room-based) location
     * @param oldTile
     * @param newTile
     */
    Room.prototype.replaceRoomTileLocal = function (oldTile, newTile) {
        var location = oldTile.location;
        this.tiles[location.y][location.x] = newTile;
    };

    /**
     * Replaces oldTile with newTile when oldTile has a global location
     * @param oldTile
     * @param newTile
     */
    Room.prototype.replaceRoomTileGlobal = function (oldTile, newTile) {
        var location = this.deGlobalizeLocation(oldTile.location);
        this.tiles[location.y][location.x] = newTile;
    };

    /**
     * Returns whether the tile passed is a floor tile or not
     * @param tile
     * @returns {boolean}
     */
    Room.prototype.isFloorTile = function (tile) {
        return tile.getType() === TileTypes.Floor;
    };

    /**
     * Returns all floor tiles in the room
     * @returns {Array.<Tile>}
     */
    Room.prototype.getFloorTiles = function () {
        var tiles = [];
        for (var i = 0; i < this.tiles.length; i++) {
            tiles = tiles.concat(this.tiles[i]);
        }
        return tiles.filter(this.isFloorTile);
    };

    /**
     * Returns a random floor Tile
     * @returns {Tile}
     */
    Room.prototype.getRandomFloorTile = function () {
        var floorTiles = this.getFloorTiles();
        var randomTileIndex = Utilities.generateRandomNumber(0, floorTiles.length - 1);
        return floorTiles[randomTileIndex];
    };

    /**
     * Adds an up staircase to this room. Location must be de-globalized because this happens after room generation
     * @returns {Staircase}
     */
    Room.prototype.addUpStaircase = function () {
        var tile = this.getRandomFloorTile();
        var stair = this.tileFactory.getUpStair(tile.location.y, tile.location.x);
        this.staircases.push(stair);
        this.replaceRoomTileGlobal(tile, stair);
        return stair;
    };

    /**
     * Adds a down staircase to this room. Location must be de-globalized because this happens after room generation
     * @returns {Staircase}
     */
    Room.prototype.addDownStaircase = function () {
        var tile = this.getRandomFloorTile();
        var stair = this.tileFactory.getDownStair(tile.location.y, tile.location.x);
        this.staircases.push(stair);
        this.replaceRoomTileGlobal(tile, stair);
        return stair;
    };

    /**
     * If a chest is spawned, pick a tile and place it there
     * @param chance
     */
    Room.prototype.placeChests = function (chance) {
        var chestPlacementChance = chance || this.chestChance;

        if (Utilities.testWithProbability(chestPlacementChance)) {
            var chestTile = this.getRandomFloorTile();
            this.tiles[chestTile.location.y][chestTile.location.x] = this.tileFactory.getChestTile(chestTile.location.y, chestTile.location.x);
            this.chests.push(chestTile);
        }
    };

    return Room;
});
