/**
 * @module MapFactory
 */
define(
    [
        'GameMap',
        'Hallway',
        'Room',
        'Staircase',
        'TileFactory',
        'TileTypes',
        'Utilities'
    ],
    function (GameMap, Hallway, Room, Staircase, TileFactory, TileTypes, Utilities) {
        "use strict";

        /**
         * Creates GameMaps
         * @alias module:Map
         * @constructor
         */
        var MapFactory = function (height, width) {
            /**
             * The actual map that will be returned
             * @type {GameMap}
             */
            this.map = null;

            /**
             * Dimensions of the GameMap to be made
             * @type {number}
             */
            this.height = height;
            this.width = width;

            /**
             * Number of rooms the GameMap will have
             * @type {number}
             */
            this.numberOfRooms = 2;

            /**
             * An array of rooms that will be placed on the GameMap
             * @type {Array.Room}
             */
            this.rooms = [];

            /**
             * An array of the stairs in the GameMap
             * @type {Array}
             */
            this.stairs = [];

            this.hallwaysGenerated = 0;

            /**
             * The min and max dimensions of the GameMap
             * @type {number}
             */
            this.minX = 1;
            this.minY = 1;
            this.maxX = this.width - 1;
            this.maxY = this.height - 1;

            /**
             * Creates Tiles
             * @type {TileFactory}
             */
            this.tileFactory = new TileFactory();
        };

        /**
         * Generates and returns a new GameMap
         * @returns {GameMap}
         */
        MapFactory.prototype.getRandomMap = function (rooms) {
            var newMap = null;
            if (rooms) {
                this.numberOfRooms = rooms;
                this.getBlankMap();
                this.generateRooms();
                this.placeStairs();
                this.generateHallways();
                this.updateBlankTiles();
                newMap = this.createMap();
                this.setBookKeepingValues(newMap);
                this.reset();
            }

            return newMap;
        };

        /**
         * Resets the factory to generate another map
         */
        MapFactory.prototype.reset = function () {
            this.map = null;
            this.numberOfRooms = null;
            this.rooms = [];
            this.stairs = [];
        };

        /**
         * Invokes a new instantiation of GameMap with this MapFactory's params
         * @returns {GameMap}
         */
        MapFactory.prototype.createMap = function () {
            return new GameMap(this.width, this.height, this.map, this.rooms, this.stairs);
        };

        /**
         * Returns a 2D array of tiles with the dimensions this.height and this.width
         * @returns {Array.Tile}
         */
        MapFactory.prototype.getBlankMapTiles = function () {
            var newMap = [];

            for (var i = 0; i < this.height; i++) {
                newMap[i] = [];
                for (var j = 0; j < this.width; j++) {
                    newMap[i][j] = this.tileFactory.getBlankTile(i, j);
                }
            }
            this.map = newMap;
        };

        /**
         * Returns a blank GameMap with the dimensions this.height and this.width
         */
        MapFactory.prototype.getBlankMap = function () {
            this.getBlankMapTiles();
            return new GameMap(this.width, this.height, this.map, 0, []);
        };

        /**
         * After map generation is complete, updates Blank tiles to NaturalWall
         */
        MapFactory.prototype.updateBlankTiles = function () {
            for (var i = 0; i < this.height; i ++) {
                for (var j = 0; j < this.width; j++) {
                    if ( this.map[i][j].getType() === TileTypes.Blank ) {
                        this.map[i][j].setType(TileTypes.NaturalWall);
                    }
                }
            }
        };

        /**
         * Generates a random point on the map
         * @returns {{number}}
         */
        MapFactory.prototype.generateRandomPointOnMap = function () {
            var x = Utilities.generateRandomNumber(this.minX, this.maxX);
            var y = Utilities.generateRandomNumber(this.minY, this.maxY);
            return {x: x, y: y};
        };

        /**
         * Returns whether a room fits in a location or not
         * @returns {boolean}
         */
        MapFactory.prototype.roomFitsInLocation = function (room, location) {
            if (this.fitsOnMap(room, location)) {
                return !this.isOverlappingRoom(room, location);
            }
            else {
                return false;
            }
        };

        /**
         * Returns true if the passed room fits in the passed location
         * @param room
         * @param location
         * @returns {boolean}
         */
        MapFactory.prototype.fitsOnMap = function (room, location) {
            return (((room.width + location.x) < this.maxX) && ((room.height + location.y) < this.maxY));
        };

        /**
         * Generates numberOfRooms Rooms
         */
        MapFactory.prototype.generateRooms = function () {
            for (var i = 0; i < this.numberOfRooms; i++) {
                var room;
                var location;
                do {
                    location = this.generateRandomPointOnMap();
                    room = new Room(location);
                }
                while(!this.roomFitsInLocation(room, location));

                this.rooms.push(room);
                this.placeFeature(room);
            }
        };

        /**
         * Returns whether the passed TileType is an overlapping type
         * @param tileType
         * @returns {boolean}
         */
        MapFactory.prototype.isOverlappingTileType = function (tileType) {
            return (tileType === TileTypes.Floor || tileType === TileTypes.Wall);
        };

        /**
         * Returns true if a room at location is overlapping a pre-existing room
         * @param room
         * @param location
         * @returns {boolean}
         */
        MapFactory.prototype.isOverlappingRoom = function (room, location) {
            var tiles = room.tiles;

            for (var i = 0; i < tiles.length; i++) {
                var yPosition = location.y + i;
                for (var j = 0; j < tiles[i].length; j++) {
                    var xPosition = location.x + j;
                    if (this.isOverlappingTileType(this.map[yPosition][xPosition].getType())) {
                        return true;
                    }
                }
            }
            return false;
        };

        /**
         * Places a Map Feature onto the GameMap
         * @param feature
         */
        MapFactory.prototype.placeFeature = function (feature) {
            var tiles = feature.tiles;
            var location = feature.location;

            for (var i = 0; i < tiles.length; i++) {
                var yPosition = location.y + i;
                for (var j = 0; j < tiles[i].length; j++) {
                    var xPosition = location.x + j;
                    if (feature.tiles[i][j]) {
                        this.map[yPosition][xPosition] = tiles[i][j];
                    }
                }
            }
        };

        /**
         * Returns all doors on the GameMap
         * @returns {Array}
         */
        MapFactory.prototype.getAllDoors = function () {
            var allDoors = [];

            for (var i = 0; i < this.rooms.length; i++) {
                allDoors.push.apply(allDoors, this.rooms[i].doors);
            }
            return allDoors;
        };

        /**
         * Returns a function that returns false if the passed door is in theseDoors
         * @param theseDoors
         * @returns {Function}
         */
        MapFactory.prototype.filterTheseDoors = function (theseDoors) {
            return function (door) {
                for (var thisDoor in theseDoors) {
                    if (theseDoors.hasOwnProperty(thisDoor) && theseDoors[thisDoor] === door) {
                        return false;
                    }
                }
                return true;
            }
        };

        /**
         * Will connect every door in a room to another door in another room
         */
        MapFactory.prototype.generateHallways = function () {
            var theseDoors;
            var otherDoors;
            var thisRoom;
            var allDoors = this.getAllDoors();

            // For each room
            for (var i = 0; i < this.rooms.length; i++) {
                thisRoom = this.rooms[i];
                theseDoors = thisRoom.doors;
                otherDoors = allDoors.filter(this.filterTheseDoors(theseDoors));

                var hallway;
                for (var j = 0; j < theseDoors.length; j++) {
                    var randomDoorIndex = Utilities.generateRandomNumber(0, otherDoors.length-1);
                    var endDoor = otherDoors[randomDoorIndex];
                    otherDoors.slice(randomDoorIndex);
                    hallway = new Hallway(theseDoors[j], endDoor, this.map);
                    this.placeFeature(hallway);
                    this.hallwaysGenerated++;
                }
            }
        };

        /**
         * Places one up stair and one down stair in different rooms on the map
         */
        MapFactory.prototype.placeStairs = function () {
            var subRooms = this.rooms.slice(0);
            var startRoom = Utilities.getRandomEntryFromArray(subRooms);
            var index = subRooms.indexOf(startRoom);
            subRooms.splice(index, 1);
            var endRoom = Utilities.getRandomEntryFromArray(subRooms);
            var upStair = startRoom.addUpStaircase();
            this.stairs.push(upStair);
            this.placeFeature(startRoom);
            var downStair = endRoom.addDownStaircase();
            this.stairs.push(downStair);
            this.placeFeature(endRoom);
        };

        /**
         * Sets the hallwaysGenerated value in the new map for testing purposes
         * @param map
         */
        MapFactory.prototype.setBookKeepingValues = function (map) {
            map.hallwaysGenerated = this.hallwaysGenerated;
        };

        return MapFactory;
    });
