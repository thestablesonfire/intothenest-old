/**
 * @module VisitedMap
 */
define([], function () {
    /**
     * A map of booleans that keeps track of whether a tile has been visited or not
     * @alias module:VisitedMap
     * @param mapTiles {Tile[][]}
     * @returns {boolean[][]}
     * @constructor
     */
    var VisitedMap = function (mapTiles) {
        var vMap = [];
        for (var i = 0; i < mapTiles.length; i++) {
            vMap [i] = [];
            for (var j = 0; j < mapTiles[0].length; j++) {
                vMap[i][j] = false;
            }
        }
        return vMap;
    };

    return VisitedMap;
});
