/**
 * @module Pathfinder
 */
define([
    'Utilities',
    'VisitedMap'
], function (Utilities, VisitedMap) {
	"use strict";
    /**
     * @alias module:Pathfinder
     * @param start - The start tile
     * @param end - The end tile
     * @param tiles - The 2D array to perform the search on
     * @constructor
     */
    var Pathfinder = function (start, end, tiles) {
		this.start = start;
		this.end = end;
		this.tiles = tiles;
		
	/**
	 * Dimensions of the map
	 * @type {Number}
	 */
	this.mapHeight = this.tiles.length;
	this.mapWidth = this.tiles[0].length;

	/**
	 * The queue of tiles to perform the search with
	 * @type {Array}
	 */
	this.queue = [];

	/**
	 * The current tile to get adjacent tiles from
	 * @type {Tile}
	 */
	this.activeTile = null;

	/**
	 * The VisitedMap for this search
	 * @type VisitedMap
	 */
	this.visitedMap = new VisitedMap(tiles);
    };

	/**
	 * Adds the first tile to the queue and starts the search
	 * @returns {Array|boolean}
	 */
	Pathfinder.prototype.findPath  = function () {
		var startTile = this.tiles[this.start.location.y][this.start.location.x];
		startTile.pathLength = 0;
		this.addToQueue(startTile);


		if (this.breadthFirstSearch()) {
			var path = this.tracePath();
			this.clearTiles();
			return path;
		}
	};

	/**
	 * Sets the tile to visited, sets the tile's previous tile and adds it to the queue
	 * @param tile
	 */
	Pathfinder.prototype.addToQueue = function (tile) {
		this.setTileVisited(tile);
		if (this.activeTile) {
			tile.previous = this.activeTile;
			tile.pathLength = tile.previous.pathLength + 1;
		}
		this.queue.push(tile);
	};

    /**
	 * Resets Tiles back to their initial state
     */
	Pathfinder.prototype.clearTiles = function () {
		var row;
		for(var i = 0; i < this.mapHeight; i++) {
			row = '';
			for(var j = 0; j < this.mapWidth; j++) {
				delete this.tiles[i][j].previous;
				delete this.tiles[i][j].pathLength;
				delete this.tiles[i][j].traced;
			}
		}
	};

	/**
	 * Marks the tile as visited in the visitedMap
	 * @param tile
	 */
	Pathfinder.prototype.setTileVisited = function (tile) {
		var x = tile.location.x;
		var y = tile.location.y;
		this.visitedMap[y][x] = true;
	};

	/**
	 * Returns whether tile has been visited
	 * @param tile
	 * @returns {boolean}
	 */
	Pathfinder.prototype.tileHasBeenVisited = function (tile) {
		var x = tile.location.x;
		var y = tile.location.y;
		return this.visitedMap[y][x];
	};

	/**
	 * Returns true if tile.type is a passable material
	 * @param tile
	 * @returns {boolean}
	 */
	Pathfinder.prototype.tileIsPassable = function (tile) {
		return tile.isPassable();
	};

	/**
	 * Gets activeTile's eligible surrounding tiles and adds them to the queue
	 */
	Pathfinder.prototype.addTiles = function () {
		var eligibleTiles = this.getEligibleSurroundingTiles(this.activeTile, false);
		var thisTile;

		for (var tile in eligibleTiles) {
			thisTile = eligibleTiles[tile];
			this.addToQueue(thisTile);
		}
	};

	/**
	 * Returns true if the location exists
	 * @param location
	 * @returns {boolean}
	 */
	Pathfinder.prototype.locationIsOnMap = function (location) {
		return (location.y >= 0 && location.y <= this.mapHeight-1) &&
			(location.x >= 0 && location.x <= this.mapWidth-1);
	};

	/**
	 * Returns true if tile at location is the end or if it is eligible to add to the queue
	 * @param location
	 * @returns {boolean}
	 */
	Pathfinder.prototype.checkTile = function (location) {
		var tile = this.tiles[location.y][location.x];
		if (this.isEndTile(tile)) {
			return true;
		}
		return (this.tileIsPassable(tile) && !this.tileHasBeenVisited(tile));
	};

	/**
	 * Returns true if tile is end
	 * @param tile
	 * @returns {boolean}
	 */
	Pathfinder.prototype.isEndTile = function (tile) {
		return this.end.hasSameLocation(tile);
	};

	/**
	 * Gets the active tile's adjacent tiles and returns only the eligible ones
	 * @returns {Array}
	 */
	Pathfinder.prototype.getEligibleSurroundingTiles = function (tile, canBeVisited) {
		var walkableSurroundingTiles = [];
		var locations = tile.getAdjacentTiles();
		var thisLocation;
		var pushTile;

		for ( var location in locations ) {
			if (locations.hasOwnProperty(location)) {
                thisLocation = locations[location];
			}
			if (this.locationIsOnMap(thisLocation) ) {
				if (canBeVisited || (!canBeVisited && this.checkTile(thisLocation))) {
					pushTile = this.tiles[thisLocation.y][thisLocation.x];
					walkableSurroundingTiles.push(pushTile);
				}
			}
		}
		return walkableSurroundingTiles;
	};
    //
	// /**
	//  * A debug method that prints the queue from front to back
	//  */
	// Pathfinder.prototype.printQueue = function () {
	// 	printTileArray(queue, 'Q');
	// };
    //
	// /**
	// * A debug method that prints an array of tiles with a symbol after each one
	// */
	// Pathfinder.prototype.printTileArray = function (arr, symbol) {
	// 	for (var tile in arr) {
	// 		if (arr.hasOwnProperty(tile)) {
     //            console.log(arr[tile].location, arr[tile].pathLength, symbol);
	// 		}
	// 	}
	// };

	/**
	 * Returns the Tile adjacent to tile that has the shortest path value
	 * @param tile
	 * @returns Array.Tile
	 */
	Pathfinder.prototype.getShortestPathTile = function (tile) {
		var surroundingTiles = this.getEligibleSurroundingTiles(tile, true);

		surroundingTiles = surroundingTiles.filter(function (tile) {
			return (!tile.traced && tile.hasOwnProperty('pathLength'));
		});

		surroundingTiles.sort(function (a,b) {
			return a.pathLength - b.pathLength;
		});

		return surroundingTiles[0];
	};

	/**
	 * Returns an array of tiles from end to start
	 * @returns {Array}
	 */
	Pathfinder.prototype.tracePath = function () {
		var currentStep = this.tiles[this.end.location.y][this.end.location.x];
		var path = [];

		do {
			currentStep.traced = true;
			path.push(currentStep);
			currentStep = this.getShortestPathTile(currentStep);
		} while (!currentStep.hasSameLocation(this.start));

		path.shift();
		return path;
	};

	/**
	 * Performs a breadth-first search for the end
	 * @returns {Tile[]|boolean}
	 */
	Pathfinder.prototype.breadthFirstSearch = function () {
		for (var count = 0; count < 500000; count++) {
			this.activeTile = this.queue.shift();
			if (this.activeTile) {
				if (this.isEndTile(this.activeTile)) {
					return true;
				}
				this.addTiles();
			} else {
				return false;
			}
		}
	};
	
    return Pathfinder;
});
