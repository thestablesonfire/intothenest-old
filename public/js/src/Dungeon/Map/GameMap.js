/**
 * Holds one level of the game map
 * @module GameMap
 */
define(['Directions','TileTypes'], function (Directions,TileTypes) {
    "use strict";

    /**
     * @alias module:GameMap
     * @param width {number}
     * @param height {number}
     * @param tiles {Array.Tile[]}
     * @param rooms {Room[]}
     * @param stairs {Array.Staircase}
     * @constructor
     */
    var GameMap = function (width, height, tiles, rooms, stairs) {
        /**
         * The height of the map
         * @type {number}
         */
        this.height = height;

        /**
         * The Player currently on the map
         * @type {null}
         */
        this.player = null;

        /**
         * Holds references to the stair tiles, should hold 2 stairs. StairsDown, then StairsUp
         * @type {Array.Staircase}
         */
        this.stairs = stairs;

        /**
         * Array of Rooms on the map
         * @type {Room[]}
         */
        this.rooms = rooms;

        /**
         * Tiles that make up the GameMap
         * @type {Array.Tile[]}
         */
        this.tiles = tiles;

        /**
         * The width of the map
         * @type {number}
         */
        this.width = width;

        /**
         * The count of the number of hallways generated
         * @type {number}
         */
        this.hallwaysGenerated = 0;

        this.DIRECTION_NORTH = 'North';
        this.DIRECTION_EAST = 'East';
        this.DIRECTION_SOUTH = 'South';
        this.DIRECTION_WEST = 'West';
    };

    /**
     * Sets the player for this GameMap
     * @param player
     */
    GameMap.prototype.setPlayer = function (player) {
        this.player = player;
        var stair = this.getUpStair();
        player.setLocation(stair.location);
    };

    /**
     * Returns the up stair on this map or a 0,0 location
     * @returns {{number}}
     */
    GameMap.prototype.getUpStair = function () {
        return this.stairs[0] || {location: {x: 0, y: 0}}
    };

    /**
     * Returns the down stair on this map or a 0,0 location
     * @returns {*}
     */
    GameMap.prototype.getDownStair = function () {
        return this.stairs[1];
    };

    /**
     * Returns whether the tile is a StairsDown Tile or not
     * @param location
     * @returns {boolean}
     */
    GameMap.prototype.tileIsDownStair = function (location) {
        var tile = this.tiles[location.y][location.x];
        return this.tileIsOfType(tile, TileTypes.StairsDown);
    };

    /**
     * Returns whether the tile is a StairsUp Tile or not
     * @param location
     * @returns {boolean}
     */
    GameMap.prototype.tileIsUpStair = function (location) {
        var tile = this.tiles[location.y][location.x];
        return this.tileIsOfType(tile, TileTypes.StairsUp);
    };

    /**
     * Generic checker to see if a tile is of a type
     * @param tile
     * @param type
     * @returns {boolean}
     */
    GameMap.prototype.tileIsOfType = function (tile, type) {
        return tile.getType() === type;
    };

    /**
     * Returns whether the tile is passable or not
     * @param location
     * @returns {boolean}
     */
    GameMap.prototype.tileIsPassable = function (location) {
        return this.getTile(location).isPassable();
    };

    /**
     * Returns the Tile object at the location specified
     * @param location
     * @returns {*}
     */
    GameMap.prototype.getTile = function (location) {
        if (this.locationIsOnMap(location)) {
            return this.tiles[location.y][location.x];
        }
    };

    /**
     * Returns a tile from a passed direction
     * @param location
     * @param direction
     * @returns {Tile}
     */
    GameMap.prototype.getTileFromDirection = function (location, direction) {
        var tile = null;
        var change = {
            x: location.x,
            y: location.y
        };

        if (direction === this.DIRECTION_NORTH) {
            change.y--;
        } else if (direction === this.DIRECTION_EAST) {
            change.x++;
        } else if (direction === this.DIRECTION_SOUTH) {
            change.y++;
        } else if (direction === this.DIRECTION_WEST) {
            change.x--;
        }

        if (this.tiles[change.y][change.x]) {
            tile = this.tiles[change.y][change.x];
        }

        return tile;
    };

    /**
     * Returns the Tiles adjacent to location
     * @param location
     * @returns {Tile[]}
     */
    GameMap.prototype.getAdjacentTiles = function (location) {
        var thisTile = this.tiles[location.y][location.x];
        var adjacentExistingTiles = thisTile.getAdjacentTiles().filter(this.locationIsOnMap, this);
        return adjacentExistingTiles.map(this.getTileFromLocation, this);
    };

    /**
     *
     * @param location
     * @returns {Tile[]}
     */
    GameMap.prototype.getAdjacentOpenables = function (location) {
        var adjacentTiles = this.getAdjacentTiles(location);
        return adjacentTiles.filter(function (tile) {
            return tile.isOpenable;
        });
    };

    /**
     * Returns single Tile at location
     * @param location
     * @returns {Tile}
     */
    GameMap.prototype.getTileFromLocation = function (location) {
        return this.tiles[location.y][location.x];
    };

    /**
     * Returns true if the location passed exists on this GameMap
     * @param location
     * @returns {boolean}
     */
    GameMap.prototype.locationIsOnMap = function (location) {
        var locX = location.x;
        var locY = location.y;
        return (locY >= 0 && locY < this.height && locX >= 0 && locX < this.width);
    };

    GameMap.prototype.setFog = function () {
        for (var i = 0; i < this.tiles.length; i++) {
            for (var j = 0; j < this.tiles[i].length; j++) {
                this.tiles[i][j].isFogged = true;
            }
        }
    };

    /**
     * Prints the game map to console
     * @returns {string}
     */
    GameMap.prototype.print = function () {
        var mapHTML = '';

        var PLAYER_SYMBOL = '<span class="player">☺</span>';
        var SINGLE_SPACE = '<span class="blank">&nbsp;</span>';

        var row;
        for(var i = -1; i < this.height; i++) {
            row = '';
            for(var j = -1; j < this.width; j++) {
                if (i !== -1 && j !== -1 && this.tiles[i][j].isVisible) {
                    if (this.player.location.y === i && this.player.location.x === j) {
                        // The space where the Player is located
                        row += PLAYER_SYMBOL;
                    } else if (i !== -1 && j !== -1) {
                        // Print Tile symbol
                        row += this.tiles[i][j].getSymbol();
                    } else if (j === -1) {
                        // If column legend
                        // If first location, leave space
                        if (i === -1) {
                            row += SINGLE_SPACE;
                        } else {
                            // Otherwise print the column number
                            row += i % 10;
                        }
                    } else {
                        // print row number
                        row += j % 10;
                    }
                } else {
                    row += SINGLE_SPACE;
                }
            }
            mapHTML += row;
            mapHTML += '<br>';
        }

        return mapHTML;
    };

    return GameMap;
});
