/**
 * @module _tileTypes
 */
define(['TileType'], function(TileType) {
    /**
     * An object containing the different TileTypes
     * @alias module:_tileTypes
     * @property Blank {TileType}
     * @property ChestClosed {TileType}
     * @property ChestOpen {TileType}
     * @property DoorClosed {TileType}
     * @property DoorOpen {TileType}
     * @property Floor {TileType}
     * @property StairsDown {TileType}
     * @property StairsUp {TileType}
     * @property Hallway {TileType}
     * @property NaturalWall {TileType}
     * @property Wall {TileType}
     */
    var TileTypes = {
        Blank: new TileType('.', true, 'an empty void', ['blank']),
        ChestClosed: new TileType('Ç', false, 'a small wooden chest', ['chest', 'closed']),
        ChestOpen: new TileType('ç', true, 'a small wooden chest', ['chest', 'open']),
        DoorClosed: new TileType('D', false, 'a closed door', ['door', 'closed']),
        DoorOpen: new TileType('O', true, 'an open door', ['door', 'open']),
        Floor: new TileType('_', true, 'an empty, floored space', ['floor']),
        StairsDown: new TileType('v', true, 'a downward staircase', ['stairs', 'down']),
        StairsUp: new TileType('^', true, 'an upward staircase', ['stairs', 'up']),
        Hallway: new TileType('H', true, 'a hallway', ['hallway']),
        NaturalWall: new TileType('*', false, 'a natural stone wall', ['wall', 'natural']),
        Wall: new TileType('#', false, 'a smoothed stone wall', ['wall'])
    };

    return TileTypes
});
