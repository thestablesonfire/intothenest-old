/**
 * @module TileType
 */
define([], function () {
    /**
     * Describes a Type of Tile
     * @param symbol
     * @param passable
     * @param description
     * @param classes
     * @constructor
     */
    var TileType = function (symbol, passable, description, classes) {
        this.symbol = symbol;
        this.passable = passable;
        this.description = description;
        this.classes = classes;
    };
    return TileType;
});
