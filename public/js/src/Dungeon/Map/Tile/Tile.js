/**
 * @module Tile
 */
define([], function () {
    var count = 0;

    /**
     * @alias module:Tile
     * @param newType
     * @param y
     * @param x
     * @constructor
     */
    var Tile = function (newType, y, x) {
        this.id = count++;
        this.type = newType;
        this.location = {
            x: x,
            y: y
        };
        this.isVisible = false;
        this.isFogged = true;
    };

    /**
     * Returns a description of the TileType
     * @returns {string}
     */
    Tile.prototype.getDescription = function () {
        return this.type.description;
    };

    /**
     * Returns the symbol for a tile type
     * @returns {string}
     */
    Tile.prototype.getSymbol = function () {
        var symbolHTML = '<span class="';
        for (var htmlClass in this.type.classes) {
            symbolHTML += this.type.classes[htmlClass] + ' ';
        }

        if (this.isFogged) {
            symbolHTML += 'fogged';
        }
        symbolHTML += '">' + this.type.symbol + '</span>';
        return symbolHTML;
    };

    /**
     * Returns the TileType
     * @returns {TileType}
     */
    Tile.prototype.getType = function () {
        return this.type;
    };

    /**
     * Returns the 4 Tile locations that are adjacent to this one
     * @returns {Array}
     */
    Tile.prototype.getAdjacentTiles = function () {
        var topLocation = {
            x: this.location.x,
            y: this.location.y - 1
        };

        var rightLocation = {
            x: this.location.x + 1,
            y: this.location.y
        };

        var bottomLocation = {
            x: this.location.x,
            y: this.location.y + 1
        };

        var leftLocation = {
            x: this.location.x - 1,
            y: this.location.y
        };

        return [
            topLocation,
            rightLocation,
            bottomLocation,
            leftLocation
        ];
    };

    /**
     * Returns all 8 surrounding tiles
     */
    Tile.prototype.getSurroundingTiles = function () {
        var surroundingTiles = this.getAdjacentTiles();

        // Northeast
        surroundingTiles.push({
            x: this.location.x + 1,
            y: this.location.y - 1
        });

        // Southeast
        surroundingTiles.push({
            x: this.location.x + 1,
            y: this.location.y + 1
        });

        // Southwest
        surroundingTiles.push({
            x: this.location.x - 1,
            y: this.location.y + 1
        });

        // Northwest
        surroundingTiles.push({
            x: this.location.x - 1,
            y: this.location.y - 1
        });

        return surroundingTiles;
    };

    /**
     * Sets a new TileType for this Tile
     * @param newType
     */
    Tile.prototype.setType = function (newType) {
        this.type = newType;
    };

    /**
     * Returns true if passable
     * @returns {boolean}
     */
    Tile.prototype.isPassable = function () {
        return this.type.passable;
    };

    /**
     * Comparator function for tile locations
     * @param tile
     * @returns {boolean}
     */
    Tile.prototype.hasSameLocation = function (tile) {
        return (this.location.x === tile.location.x) && (this.location.y === tile.location.y);
    };

    return Tile;
});
