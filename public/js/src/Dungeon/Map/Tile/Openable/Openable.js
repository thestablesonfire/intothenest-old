/**
 * @module Openable
 */
define(['Tile'], function (Tile) {
    /**
     * @alias module:Openable
     * @param type {TileType}
     * @param y {number}
     * @param x {number}
     * @constructor
     */
    var Openable = function (type, y, x) {
        /**
         * Denotes an openable object
         * @type {boolean}
         */
        this.isOpenable = true;

        /**
         * Message that describes the last action taken
         * @type {string}
         */
        this.resultsMessage = '';

        Tile.call(this, type, y, x);
    };

    Openable.prototype = Object.create(Tile.prototype);

    /**
     * Returns a function that will set a tile to a new type
     * @param newType
     * @param failMessage
     * @param successMessage
     * @returns {Function}
     */
    Openable.prototype.actionFactory = function(newType, failMessage, successMessage) {
        return function() {
            if (this.type === newType) {
                this.resultsMessage = failMessage;
                return false;
            }
            this.type = newType;
            this.resultsMessage = successMessage;
            return true;
        };
    };

    /**
     * Returns the current resultsMessage which is set by the last operation
     * @returns {string}
     */
    Openable.prototype.getResultMessage = function () {
        var msg = this.resultsMessage;
        this.resultsMessage = '';
        return msg;
    };

    /**
     * An openable must open
     */
    Openable.prototype.open = function () {};

    /**
     * An openable must close
     */
    Openable.prototype.close = function () {};

    return Openable;
});
