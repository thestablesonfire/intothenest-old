/**
 * @module Chest
 */
define(['Openable', 'TileTypes'], function (Openable, TileTypes) {
    /**
     * @alias module:Chest
     * @constructor
     */
    var Chest = function (y,x) {
        this.className = 'Chest';
        this.inventory = [];

        Openable.call(this, TileTypes.ChestClosed, y, x);
    };
    Chest.prototype = Object.create(Openable.prototype);

    /**
     * Describe the various outcomes of trying to open/close this Chest
     * @type {string}
     */
    var CHEST_OPENED_SUCCESS = 'The chest was opened.';
    var CHEST_CLOSED_SUCCESS = 'The chest was closed.';
    var CHEST_ALREADY_OPENED = 'This chest is already open.';
    var CHEST_ALREADY_CLOSED = 'This chest is already closed.';

    /**
     * Opens this Chest
     * @type {Function}
     */
    Chest.prototype.open = Chest.prototype.actionFactory(TileTypes.ChestOpen, CHEST_ALREADY_OPENED, CHEST_OPENED_SUCCESS);

    /**
     * Closes this Chest
     * @type {Function}
     */
    Chest.prototype.close = Chest.prototype.actionFactory(TileTypes.ChestClosed, CHEST_ALREADY_CLOSED, CHEST_CLOSED_SUCCESS);

    return Chest;
});
