/**
 * @module Door
 */
define(['Openable', 'TileTypes'], function (Openable, TileTypes) {
    /**
     * @alias module:Door
     * @extends Tile
     * @param y {number}
     * @param x {number}
     * @constructor
     */
    function Door (y,x) {
        /**
         * Used for type checks since JS inheritance is stupid
         * @type {string}
         */
        this.className = 'Door';

        Openable.call(this, TileTypes.DoorClosed, y, x);
    }

    Door.prototype = Object.create(Openable.prototype);

    /**
     * Messages that describe various results
     * @type {string}
     */
    var DOOR_OPENED_SUCCESS = 'The door was opened.';
    var DOOR_CLOSED_SUCCESS = 'The door was closed.';
    var DOOR_ALREADY_OPENED = 'This door is already open.';
    var DOOR_ALREADY_CLOSED = 'This door is already closed.';

    /**
     * Opens the door if it is not already opened, returns a description of the outcome
     * @returns {string}
     */
    Door.prototype.open = Door.prototype.actionFactory(TileTypes.DoorOpen, DOOR_ALREADY_OPENED, DOOR_OPENED_SUCCESS);

    /**
     * Closes the door if it is not already closed, returns a description of the outcome
     * @returns {string}
     */
    Door.prototype.close = Door.prototype.actionFactory(TileTypes.DoorClosed, DOOR_ALREADY_CLOSED, DOOR_CLOSED_SUCCESS);

    return Door;
});
