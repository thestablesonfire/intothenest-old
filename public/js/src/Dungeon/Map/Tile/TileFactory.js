/**
 * @module TileFactory
 */
define(['Chest', 'Door', 'Staircase', 'Tile', 'TileTypes'], function (Chest, Door, Staircase, Tile, TileTypes) {
    /**
     * Creates Tiles
     * @alias module:TileFactory
     * @constructor
     */
    var TileFactory = function () {};

    /**
     * Returns a new Blank Tile
     */
    TileFactory.prototype.getBlankTile = tileGetterFactory(TileTypes.Blank);

    /**
     * Returns a new Floor Tile
     */
    TileFactory.prototype.getFloorTile = tileGetterFactory(TileTypes.Floor);

    /**
     * Returns a new Hallway Tile
     */
    TileFactory.prototype.getHallwayTile = tileGetterFactory(TileTypes.Hallway);

    /**
     * Returns a new Wall Tile
     */
    TileFactory.prototype.getWallTile = tileGetterFactory(TileTypes.Wall);

    /**
     * Returns a new Up Stair Tile
     * @param y
     * @param x
     * @returns {*}
     */
    TileFactory.prototype.getUpStair = function (y, x) {
        return new Staircase(TileTypes.StairsUp, y, x);
    };

    /**
     * Returns a new Down Stair Tile
     * @param y
     * @param x
     * @returns {*}
     */
    TileFactory.prototype.getDownStair = function (y, x) {
        return new Staircase(TileTypes.StairsDown, y, x);
    };

    /**
     * Returns a new Door Tile
     * @param y
     * @param x
     * @returns {*}
     */
    TileFactory.prototype.getDoorTile = function (y, x) {
        return new Door(y, x);
    };

    /**
     * Returns a new Blank Tile
     */
    TileFactory.prototype.getChestTile = function (y, x) {
        return new Chest(y, x);
    };

    /**
     * Returns a function that will make new Tiles of the passed type
     * @param type
     * @returns {Function}
     */
    function tileGetterFactory(type) {
        return function (y, x) {
            return new Tile(type, y, x);
        };
    }

    return TileFactory;
});
