/**
 * @module Staircase
 */
define(['Tile'], function (Tile) {
    /**
     * A Tile that allows travel from one Map to another
     * @alias module:Staircase
     * @param type
     * @param y
     * @param x
     * @constructor
     */
    var Staircase = function (type, y, x) {
        Tile.call(this, type, y, x);
    };

    Staircase.prototype = Object.create(Tile.prototype);

    return Staircase;
});
