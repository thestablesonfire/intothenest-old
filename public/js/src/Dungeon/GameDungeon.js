/**
 * @module GameDungeon
 */
define(['MapFactory'], function (MapFactory) {
    /**
     * The collection of GameMaps
     * @constructor
     */
    var GameDungeon = function (player) {
        /**
         * Generates GameMaps
         * @type {MapFactory}
         */
        this.mapFactory = new MapFactory(30,30);

        /**
         * Holds the GameMaps
         * @type {Array.GameMap}
         */
        this.maps = [];

        /**
         * Index indicating the current map
         * @type {number}
         */
        this.curMapIndex = -1;

        /**
         * The Player obj
         * @type {Player}
         */
        this.player = player;
    };

    /**
     * Initialization
     */
    GameDungeon.prototype.init = function () {
        var newMap = this.getNewMap();
        this.addMap(newMap);
        this.curMapIndex++;
        this.setupPlayer();
    };

    /**
     * Runs the setup for the Player
     */
    GameDungeon.prototype.setupPlayer = function () {
        this.updatePlayerLocation();
        this.player.setDungeonContext(this);
        this.player.setDownStairCallback(this.playerMovedDown);
        this.player.setUpStairCallback(this.playerMovedUp);
    };

    /**
     * Tells the current GameMap what the Player object is, and tells the Player what maps it is on
     */
    GameDungeon.prototype.updatePlayerLocation = function () {
        var currentMap = this.getCurrentMap();
        this.player.setMap(currentMap);
        currentMap.setPlayer(this.player);
    };

    /**
     * The Player has ascended a flight of stairs
     */
    GameDungeon.prototype.playerMovedUp = function () {
        // If you're not on the top level
        if (this.curMapIndex - 1 >= 0) {
            this.curMapIndex--;
            this.updatePlayerLocation();
            this.player.setLocation(this.getCurrentMap().getDownStair().location);
            return true;
        }
        return false;
    };

    /**
     * The Player has descended a flight of stairs
     */
    GameDungeon.prototype.playerMovedDown = function () {
        var newMap = this.playerIsOnLowestDiscoveredLevel();
        this.curMapIndex++;
        if (newMap) {
            this.addMap(this.getNewMap());
        }
        this.updatePlayerLocation();
        return true;
    };

    /**
     * Returns whether the Player is on the lowest discovered level
     * @returns {boolean}
     */
    GameDungeon.prototype.playerIsOnLowestDiscoveredLevel = function () {
        return (this.curMapIndex === this.maps.length - 1);
    };

    /**
     * Prints the current GameMap
     * @returns {string}
     */
    GameDungeon.prototype.printCurrentMap = function () {
        return this.getCurrentMap().print();
    };

    /**
     * Returns the current GameMap
     * @returns {GameMap}
     */
    GameDungeon.prototype.getCurrentMap = function () {
        return this.maps[this.curMapIndex];
    };

    /**
     * Creates a new GameMap
     * @returns {GameMap}
     */
    GameDungeon.prototype.getNewMap = function () {
        return this.mapFactory.getRandomMap(5);
    };

    /**
     * Adds a new GameMap to this.maps
     * @param map {GameMap}
     */
    GameDungeon.prototype.addMap = function (map) {
        this.maps.push(map);
        this.linkStairs();
    };

    /**
     * Links a Down Staircase on the current level with the Up Staircase on the newest level
     */
    GameDungeon.prototype.linkStairs = function () {
        var newIndex = this.maps.length - 1;
        var oldIndex = newIndex - 1;

        if (oldIndex >= 0) {
            var newMap = this.maps[newIndex];
            var oldMap = this.maps[oldIndex];

            var newStair = newMap.getUpStair();
            var oldStair = oldMap.getDownStair();

            newStair.link = oldStair;
            oldStair.link = newStair;
        }
    };

    return GameDungeon;
});
