/**
 * @module IntoTheNest
 */
define(['GameDungeon', 'InputHandler', 'MapFactory', 'Player'], function (GameDungeon, InputHandler, MapFactory, Player) {
    /**
     * The main game container
     * @alias module:IntoTheNest
     * @constructor
     */
    var IntoTheNest = function (input, output, mapDisplay) {
        /**
         * @type {MapFactory}
         */
        var mapFactory = new MapFactory(25, 25);

        /**
         * @type {{jQuery}}
         */
        var $inputSource = input;

        /**
         * @type {{jQuery}}
         */
        var $outputDisplay = output;

        /**
         * The HTML map display section
         * @type {{jQuery}}
         */
        var $mapDisplay = mapDisplay;

        /**
         * The container for the output list, needed to scroll to new response
         * @type {jQuery}
         */
        var $outputContainer = $($outputDisplay).parent();

        /**
         * @type {Player}
         */
        var player = new Player();

        /**
         * Collection of maps for this game
         * @type {GameDungeon}
         */
        var dungeon = new GameDungeon(player);
        dungeon.init();

        /**
         * Initialization method
         */
        this.init = function () {
            this.InputHandler = new InputHandler(player, writeToOutput);
            updateMap(dungeon.printCurrentMap());
            $inputSource.focus();
        };

        /**
         * Gets value from input
         */
        this.input = function () {
            var value = $inputSource.val().toLowerCase().split(' ');
            this.InputHandler.submitInput(value);
            clearInputField();
            updateMap(dungeon.printCurrentMap());
        };

        /**
         * Clears the input field after a command is submitted
         */
        function clearInputField() {
            $inputSource.val('');
        }

        /**
         * Scrolls the output list to the newest response at the bottom
         */
        function scrollResponseList() {
            $($outputContainer).scrollTop( $($outputDisplay).height() );
        }

        /**
         * Writes outputMsg to $outputDisplay
         * @param outputMsg
         */
        function writeToOutput(outputMsg) {
            if (outputMsg === '__CLEAR__') {
                clearConsole();
            } else {
                $outputDisplay.append(outputMsg);
                scrollResponseList();
            }
        }

        function clearConsole() {
            $outputDisplay.html('');
        }

        /**
         * Prints the visible map to the display
         * @param mapDisplayString
         */
        function updateMap(mapDisplayString) {
            $mapDisplay.html(mapDisplayString);
        }
    };

    return IntoTheNest;
});
