/**
* @module Utilities
*/
define([], function () {
    /**
    * Generates a random number between min and max
    * @alias module:Utilities
    * @constructor
    */
    var Utilities = function () {};

    /**
     * Generates a random number between min and max
     * @param min
     * @param max
     * @returns {*}
     */
    Utilities.prototype.generateRandomNumber = function (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    };

    /**
     * Returns a property name if the string is in the array of a property in the object
     * @param string
     * @param object
     * @returns {*}
     */
    Utilities.prototype.stringIsInObject = function (string, object) {
		var inputRegex = '^' + string + '$';
        for (property in object) {
            for(var option in object[property]) {
                if (object[property][option].match(inputRegex)) {
                    return property;
                }
            }
        }

        return null;
    };

    /**
     * Returns true or false based on the probability passed in
     * @param probability
     */
    Utilities.prototype.testWithProbability = function (probability) {
        return this.generateRandomNumber(0,100) <= (probability * 100);
    };

    /**
     * Returns a random entry in an array
     * @param array
     * @returns {*}
     */
    Utilities.prototype.getRandomEntryFromArray = function (array) {
        var index = this.generateRandomNumber(0, array.length-1);

        return array[index];
    };
    
    return new Utilities();
});
