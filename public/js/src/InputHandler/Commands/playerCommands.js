/**
 * @module _playerCommands
 */
define([], function () {
    /**
     * The valid commands a player can submit
     * @alias module:_playerCommands
     * @property {string[]} north - w
     * @property {string[]} east - d
     * @property {string[]} south - s
     * @property {string[]} west - a
     * @property {string[]} open - o, open
     * @property {string[]} close - c, close
     */
    var PlayerCommands = {
        north: ['w'],
		east: ['d'],
		south: ['s'],
		west: ['a'],
        open: ['o', 'open'], // open [openableType] [direction]
        close: ['c', 'close']
    };

    return PlayerCommands;
});
