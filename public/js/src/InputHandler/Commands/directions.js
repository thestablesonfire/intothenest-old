/**
 * @module _directions
 */
define([], function () {
    /**
     * An object containing the valid directions and their corresponding keywords
     * @alias module:_directions
     * @property North {string[]}
     * @property East {string[]}
     * @property South {string[]}
     * @property West {string[]}
     */
    var Directions = {
        North: ['n','north'],
        East: ['e', 'east'],
        South: ['s', 'south'],
        West: ['w', 'west']
    };

    return Directions;
});
