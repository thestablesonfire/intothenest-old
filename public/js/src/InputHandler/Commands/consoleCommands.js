/**
 * @module _consoleCommands
 */
define([], function () {
    var ConsoleCommands = {
        help: ['help'],
        clear: ['clear']
    };

    return ConsoleCommands;
});
