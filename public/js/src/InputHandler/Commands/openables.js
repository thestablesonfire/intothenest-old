/**
 * @module _openables
 */
define([], function () {
    /**
     * An object containing the different types of Openables and their corresponding keywords
     * @alias module:_openables
     * @property Chest {string[]}
     * @property Door {string[]}
     */
    var Openables = {
        'Chest': ['chest'],
        'Door': ['door']
    };

    return Openables;
});
