/**
 * @module ResponseFactory
 */
define([], function () {
    /**
     * Takes a message and a type and outputs an HTML response
     * @alias module:ResponseFactory
     * @constructor
     */
    var ResponseFactory = function () {
        this.outputBegin = '<li class="output_msg ';
        this.outputBeginClosing = '">';
        this.outputEnd = '</li>';
    };

    /**
     * Builds an HTML response from a message and message type
     * @param msg
     * @param type
     * @returns {string}
     */
    ResponseFactory.prototype.buildResponse = function (msg, type) {
        return this.outputBegin + type + this.outputBeginClosing + msg + this.outputEnd;
    };

    return ResponseFactory;
});
