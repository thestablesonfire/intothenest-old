/**
* @module InputHandler
*/
define([
    'ConsoleCommands',
    'ConsoleCommandDelegator',
    'PlayerCommands',
    'PlayerCommandDelegator',
    'ResponseFactory',
    'Utilities'
], function (
    ConsoleCommands,
    ConsoleCommandDelegator,
    PlayerCommands,
    PlayerCommandDelegator,
    ResponseFactory,
    Utilities
) {
    /**
     * Handles the input from the user
     * @alias module:InputHandler
     * @constructor
     */
    var InputHandler = function (player, outputMethod) {
        this.currentCommand = null;
        this.consoleDelegator = new ConsoleCommandDelegator();
        this.playerDelegator = new PlayerCommandDelegator(player);
        this.output = outputMethod;
        this.responseFactory = new ResponseFactory();

        this.ERROR_CLASS = 'error';
        this.SUCCESS_CLASS = 'msg';
    };
    
   /**
    * Handles input submissions
    */
    InputHandler.prototype.submitInput = function (input) {
        var command = input[0];
        var success;

        if (this.isConsoleCommand(command)) {
            success = this.consoleDelegator[this.currentCommand]();
            this.respond(this.consoleDelegator.getResultMessage(), success)
        } else if (this.isPlayerCommand(command)) {
            success = this.playerDelegator[this.currentCommand](input);
            this.respond(this.playerDelegator.getResultMessage(), success);
        } else {
            this.respond('Command not recognized.', false);
        }
    };

    /**
     * Writes a response to the output display
     * @param msg
     */
    InputHandler.prototype.respond = function (msg, success) {
        var msgType;

        if(msg === '__CLEAR__') {
            this.output(msg);
        } else {
            success ? msgType = this.SUCCESS_CLASS : msgType = this.ERROR_CLASS;
            this.output(this.responseFactory.buildResponse(msg, msgType));
        }
    };

    /**
     * Determines whether or not the command is a console action
     * @type {Function}
     */
    InputHandler.prototype.isConsoleCommand = commandChecker(ConsoleCommands);
    
   /**
    * Determines whether or not the command is a player action
    * @returns {boolean}
    */
    InputHandler.prototype.isPlayerCommand = commandChecker(PlayerCommands);

    /**
     * A factory that generates a checker function
     * @param commandList
     * @returns {Function}
     */
    function commandChecker(commandList) {
        return function (cmd) {
            var prop = Utilities.stringIsInObject(cmd, commandList);
            if (prop) {
                this.currentCommand = prop;
                return true;
            } else {
                return false;
            }
        };
    }
    
    return InputHandler;
});
