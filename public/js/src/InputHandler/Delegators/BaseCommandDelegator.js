define([], function () {
    var BaseCommandDelegator = function () {
        this.resultMessage = '';
    };

    BaseCommandDelegator.prototype.getResultMessage = function () {
        var msg = this.resultMessage;
        this.resultMessage = '';
        return msg;
    };

    return BaseCommandDelegator;
});
