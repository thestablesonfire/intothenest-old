define(['BaseCommandDelegator'], function (BaseCommandDelegator) {
    var ConsoleDelegator = function () {
        this.resultMessage = '';
        BaseCommandDelegator.call(this);
    };

    ConsoleDelegator.prototype = Object.create(BaseCommandDelegator.prototype);

    ConsoleDelegator.prototype.help = function () {
        this.resultMessage = this.getMovementHelp();
        this.resultMessage += this.getActionsHelp();
        this.resultMessage += this.getConsoleActionsHelp();

        return true;
    };

    ConsoleDelegator.prototype.getMovementHelp = function () {
        var msg = '<li>Moving</li>';
        msg += '<li>-------------------------</li>';
        msg += '<li>w - move north</li>';
        msg += '<li>s - move south</li>';
        msg += '<li>a - move west</li>';
        msg += '<li>d - move east</li>';
        msg += '<br/>';

        return msg;
    };

    ConsoleDelegator.prototype.getActionsHelp = function () {
        var msg = '<li>Actions</li>';
        msg += '<li>-------------------------</li>';
        msg += '<li>o, open [object] [direction] - open an [object] in a specific [direction]';
        msg += ' : object and direction are optional unless there are multiple openable objects' +
            ' adjacent.<br/>';
        msg += 'Ex: open door, open west, open door east</li>';
        msg += '<li>c, close [object] [direction] - close an [object] in a specific [direction]';
        msg += ' : object and direction are optional unless there are multiple closeable objects' +
            ' adjacent<br/>';
        msg += 'Ex: close door, close west, close door east</li>';
        msg += '<br/>';

        return msg;
    };

    ConsoleDelegator.prototype.getConsoleActionsHelp = function () {
        var msg = '<li>Console</li>';
        msg += '<li>-------------------------</li>';
        msg += '<li>clear - clears the console</li>';
        msg += '<li>help - displays the help text</li>';
        msg += '<br/>';

        return msg;
    };

    ConsoleDelegator.prototype.clear = function () {
        this.resultMessage = '__CLEAR__';
        return true;
    };

    return ConsoleDelegator;
});
