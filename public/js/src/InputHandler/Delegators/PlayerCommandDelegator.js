/**
 * @module PlayerCommandDelegator
 */
define(['BaseCommandDelegator'], function (BaseCommandDelegator) {
    /**
     * Takes player commands and delegates them to the class that should handle them
     * @alias module:PlayerCommandDelegator
     * @param player
     * @constructor
     */
    var PlayerDelegator = function (player) {
        this.player = player;
        this.MOVE_FAIL = 'You were unable to move due to ';
        this.ERR_NUM_ARGS = 'An incorrect number of arguments was passed for this command';

        BaseCommandDelegator.call(this);
    };

    PlayerDelegator.prototype = Object.create(BaseCommandDelegator.prototype);

    // ===== Player movement ===== //

    /**
     * Moves the player North one tile
     * @param args
     * @returns {boolean}
     */
    PlayerDelegator.prototype.north = function (args) {
        return this.move(args, 'north', 'moveNorth');
    };

    /**
     * Moves the player East one tile
     * @param args
     * @returns {boolean}
     */
    PlayerDelegator.prototype.east = function (args) {
        return this.move(args, 'east', 'moveEast');
    };

    /**
     * Moves the player South one tile
     * @param args
     * @returns {boolean}
     */
	PlayerDelegator.prototype.south = function (args) {
        return this.move(args, 'south', 'moveSouth');
	};

    /**
     * Moves the player West one tile
     * @param args
     * @returns {boolean}
     */
	PlayerDelegator.prototype.west = function (args) {
        return this.move(args, 'west', 'moveWest');
	};

    /**
     * Attempts to move the player 1 tile in some direction
     * @param args - the arguments passed in
     * @param dir - a string describing the movement direction
     * @param fnName - the name of the function of the Player to call
     * @returns {boolean}
     */
    PlayerDelegator.prototype.move = function (args, dir, fnName) {
        if (this.correctNumberOfArgs(1, args)) {
            var moved = this.player[fnName]();
            this.resultMessage = this.player.getResultMessage();
            return moved;
        } else {
            this.resultMessage = this.ERR_NUM_ARGS;
            return false;
        }
    };

    // ===== /Player Movement ===== //

    // ===== Player Actions ===== //

    /**
     * Attempts to open an _openable for the player
     * @param args
     */
    PlayerDelegator.prototype.open = function (args) {
        var outcome = this.player.open(args[1], args[2]);
        this.resultMessage = this.player.getResultMessage();
        return outcome;
    };

    /**
     * Attempts to close an _openable for the player
     * @param args
     */
    PlayerDelegator.prototype.close = function (args) {
        var outcome = this.player.close(args[1], args[2]);
        this.resultMessage = this.player.getResultMessage();
        return outcome;
    };

    // ===== /Player Actions ===== //

    /**
     * A simple method to make sure the expected number of arguments was passed in
     * @param expected
     * @param array
     * @returns {boolean}
     */
    PlayerDelegator.prototype.correctNumberOfArgs = function (expected, array) {
        return array.length === expected;
    };

    return PlayerDelegator;
});
