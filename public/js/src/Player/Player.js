/**
 * @module Player
 */
define(['Directions','Openables', 'Utilities'], function (Directions, Openables, Utilities) {
    "use strict";

    /**
     * The Player of the game
     * @alias module:Player
     * @constructor
     */
    var Player = function () {
        this.location = {
            x: null,
            y: null
        };

        /**
         * The string describing the most recent player failed action
         * @type {string}
         */
        this.resultsMessage = '';

        /**
         * The GameMap that this player is on
         * @type {GameMap | null}
         */
        this.map = null;

        /**
         * The context for the dungeon for callbacks
         * @type {GameDungeon}
         */
        this.dungeonContext = null;

        /**
         * Called to move down a set of stairs
         * @type {Function}
         */
        this.moveDownStairs = null;

        /**
         * Called to move up a set of stairs
         * @type {Function}
         */
        this.moveUpStairs = null;
    };

    /**
     * Action messages
     * @type {string}
     */
    var NOTHING_TO_OPEN = 'There is nothing to open.';
    var NOTHING_TO_CLOSE = 'There is nothing to close.';
    var MULTIPLE_OPENABLES = 'More than one openable object nearby. Please specify a type, direction, or both.';
    var TYPE_UNOPENABLE = 'Object type passed cannot be opened.';
    var TYPE_UNCLOSABLE = 'Object type passed cannot be closed.';
    var MOVED_DOWN_STAIRS = 'You have gone down a staircase to the next level.';
    var MOVED_UP_STAIRS = 'You have gone up a staircase to the previous level.';
    var CANNOT_MOVE_UP_STAIRS = 'These stairs lead back to the surface. I must not run away.';

    /**
     * Gets the most recent player action failure string, clears it, then returns it
     * @returns {string}
     */
    Player.prototype.getResultMessage = function () {
        var reason = this.resultsMessage;
        this.resultsMessage = '';

        return reason;
    };

    /**
     * Sets the location of the player on the map
     * @param location
     */
    Player.prototype.setLocation = function (location) {
        this.location.x = location.x;
        this.location.y = location.y;
        this.revealTiles();
    };

    /**
     * Sets the reference to the GameMap for the player
     * @param map {GameMap}
     */
    Player.prototype.setMap = function (map) {
        this.map = map;
    };

    /**
     * Sets the context for the Dungeon, allowing the Player to call Dungeon callbacks
     * @param ctx
     */
    Player.prototype.setDungeonContext = function (ctx) {
        this.dungeonContext = ctx;
    };

    /**
     * Sets the callback for the Player moving down a Staircase
     * @param callback
     */
    Player.prototype.setDownStairCallback = function (callback) {
        this.moveDownStairs = callback;
    };

    /**
     * Sets the callback for the Player moving up a Staircase
     * @param callback
     */
    Player.prototype.setUpStairCallback = function (callback) {
        this.moveUpStairs = callback;
    };

    /**
     * Returns true
     * @param location
     * @returns {*}
     */
    Player.prototype.tileIsPassable = function (location) {
        return this.map.tileIsPassable(location);
    };

    /**
     * Returns true if the Tile is a StairsDown Tile
     * @param location
     * @returns {*}
     */
    Player.prototype.tileIsDownStair = function (location) {
        return this.map.tileIsDownStair(location);
    };

    /**
     * Returns true if the Tile is a StairsUp Tile
     * @param location
     * @returns {*}
     */
    Player.prototype.tileIsUpStair = function (location) {
        return this.map.tileIsUpStair(location);
    };

    /**
     * Returns true if the location exists on the current GameMap
     * @param location
     * @returns {*}
     */
    Player.prototype.locationIsOnMap = function (location) {
        return this.map.locationIsOnMap(location);
    };

    /**
     * Attempts to move the player north 1 tile
     * @returns {boolean}
     */
    Player.prototype.moveNorth = function () {
        var change = {
            x: 0,
            y: -1
        };
        return this.move(change, 'North');
    };

    /**
     * Attempts to move the player East 1 tile
     * @returns {boolean}
     */
    Player.prototype.moveEast = function () {
        var change = {
            x: 1,
            y: 0
        };
        return this.move(change, 'East');
    };

    /**
     * Attempts to move the player South 1 tile
     * @returns {boolean}
     */
    Player.prototype.moveSouth = function () {
        var change = {
            x: 0,
            y: 1
        };
        return this.move(change, 'South');
    };

    /**
     * Attempts to move the player West 1 tile
     * @returns {boolean}
     */
    Player.prototype.moveWest = function () {
        var change = {
            x: -1,
            y: 0
        };
        return this.move(change, 'West');
    };

    /**
     * Attempts to move the player, returns true if successful, if unsuccessful sets a failure reason and returns false
     * @param change
     * @returns {*}
     */
    Player.prototype.move = function (change, directionString) {
        var newLocation = {
            x: this.location.x + change.x,
            y: this.location.y + change.y
        };

        if (this.locationIsOnMap(newLocation)) {
            if (this.tileIsPassable(newLocation)) {
                // Tile exists and is passable
                this.location = newLocation;
                if (!this.checkSpecialTiles(newLocation)) {
                    this.map.setFog();
                    this.revealTiles();
                    this.resultsMessage = 'You were able to move ' + directionString + ' by 1 tile.';
                }
                return true;
            } else {
                // Tile is not passable
                var tileDescription = this.map.getTile(newLocation).getDescription();
                this.resultsMessage = tileDescription + ' blocking your way.';
                return false;
            }
        } else {
            // Tile does not exist
            this.resultsMessage = 'being at the edge of the map.';
            return false;
        }
    };

    /**
     * Checks the player's location for special properties
     * @param location
     * @returns {boolean}
     */
    Player.prototype.checkSpecialTiles = function (location) {
        if (this.tileIsDownStair(location)) {
            this.moveDownStairAction();
            return true;
        } else if (this.tileIsUpStair(location)) {
            this.moveUpStairAction();
            return true;
        }
        return false;
    };

    /**
     * Calls the moveDownStairs callback and sets the result message
     */
    Player.prototype.moveDownStairAction = function () {
        this.moveDownStairs.call(this.dungeonContext);
        this.resultsMessage = MOVED_DOWN_STAIRS;
    };

    /**
     * Calls the moveUpStairs callback and sets the result message
     */
    Player.prototype.moveUpStairAction = function () {
        if (this.moveUpStairs.call(this.dungeonContext)) {
            this.resultsMessage = MOVED_UP_STAIRS;
        } else {
            this.resultsMessage = CANNOT_MOVE_UP_STAIRS;
        }
    };

    /**
     * Returns a function that returns true if the passed tile is equal to the initially set type
     * @param type
     * @returns {Function}
     */
    Player.prototype.tileIsOfType = function (type) {
        return function (tile) {
            return tile.className === type;
        };
    };

    /**
     * Marks tiles that are visible to the Player
     */
    Player.prototype.revealTiles = function () {
        var thisTile = this.map.tiles[this.location.y][this.location.x];
        thisTile.isVisible = true;
        var surroundingTiles = thisTile.getSurroundingTiles();
        for (var tile in surroundingTiles) {
            this.revealTile(surroundingTiles[tile]);
        }
    };

    /**
     * If the tiles exists at the passed location, mark it visible
     * @param location
     */
    Player.prototype.revealTile = function (location) {
        var thisTile = this.map.tiles[location.y][location.x];

        if (thisTile) {
            thisTile.isVisible = true;
            thisTile.isFogged = false;
        }
    };

    /**
     * Attempts to open an openable with no additional information
     * @type {Function}
     */
    Player.prototype.checkOpen = openableCheckFactory('open', NOTHING_TO_OPEN, 'checkOpenType');

    /**
     * Attempts to open an openable with the type of openable
     * @type {Function}
     */
    Player.prototype.checkOpenType = openableCheckTypeFactory('open', MULTIPLE_OPENABLES, TYPE_UNOPENABLE);

    /**
     * Attempts to open an openable with the direction of the openable from the player
     * @type {Function}
     */
    Player.prototype.checkOpenDirection = openableCheckDirectionFactory('open');

    /**
     * Attempts to close an openable with no additional information
     * @type {Function}
     */
    Player.prototype.checkClose = openableCheckFactory('close', NOTHING_TO_CLOSE, 'checkCloseType');

    /**
     * Attempts to close an openable with the type of openable
     * @type {Function}
     */
    Player.prototype.checkCloseType = openableCheckTypeFactory('close', MULTIPLE_OPENABLES, TYPE_UNCLOSABLE);

    /**
     * Attempts to close an openable with the direction of the openable from the player
     * @type {Function}
     */
    Player.prototype.checkCloseDirection = openableCheckDirectionFactory('close');

    /**
     * Returns true if the argument represents a direction
     * @param arg
     */
    Player.prototype.getArgumentIsDirection = function (arg) {
        return  Utilities.stringIsInObject(arg, Directions);
    };

    /**
     * Returns true if the argument represents a type of openable
     * @param arg
     */
    Player.prototype.getArgumentIsOpenable = function (arg) {
        return Utilities.stringIsInObject(arg, Openables);
    };

    /**
     * Attempts to open the specified openable adjacent to the player
     * @type {Function}
     */
    Player.prototype.open = openableActionFactory('checkOpenDirection', 'checkOpen');

    /**
     * Attempts to close the specified openable adjacent to the player
     * @type {Function}
     */
    Player.prototype.close = openableActionFactory('checkCloseDirection', 'checkClose');

    /**
     * Creates a function that attempts to perform an action on an openable
     * @param actionName
     * @param failMessage
     * @param nextMethod
     * @returns {Function}
     */
    function openableCheckFactory(actionName, failMessage, nextMethod) {
        return function (type, direction, adjacentOpenables) {
            if (!adjacentOpenables.length) {
                // If no openables adjacent
                this.resultsMessage = failMessage;
                return false;
            } else if (adjacentOpenables.length === 1 && !type) {
                // If 1 openable adjacent
                var outcome = adjacentOpenables[0][actionName]();
                this.resultsMessage = adjacentOpenables[0].getResultMessage();
                return outcome;
            } else {
                return this[nextMethod](type, direction, adjacentOpenables);
            }
        };
    }

    /**
     * Creates a function that attempts to perform an action on an openable of a certain type
     * @param actionName
     * @param multipleMsg
     * @param unopenableMsg
     * @returns {Function}
     */
    function openableCheckTypeFactory(actionName, multipleMsg, unopenableMsg) {
        return function (type, direction, adjacentOpenables) {
            if (!type) {
                this.resultsMessage = multipleMsg;
                return false;
            } else {
                var filterFn = this.tileIsOfType(type);
                adjacentOpenables = adjacentOpenables.filter(filterFn);
                if (!adjacentOpenables.length) {
                    this.resultsMessage = 'There are no ' + type + 's to open.';
                    return false;
                } else if (adjacentOpenables.length === 1) {
                    var outcome = adjacentOpenables[0][actionName]();
                    this.resultsMessage = adjacentOpenables[0].getResultMessage();
                    return outcome;
                } else {
                    this.resultsMessage = unopenableMsg;
                    return false;
                }
            }
        };
    }

    /**
     * Attempts to perform an action on an openable in a certain direction
     * @param actionName
     * @returns {Function}
     */
    function openableCheckDirectionFactory(actionName) {
        return function (type, direction) {
            var tile = this.map.getTileFromDirection(this.location, direction);
            if (tile && tile.isOpenable) {
                var outcome = tile[actionName]();
                this.resultsMessage = tile.getResultMessage();
                return outcome;
            } else {
                this.resultsMessage = 'There is nothing to open to the ' + direction;
                return false;
            }
        };
    }

    /**
     * Function factory that produces a way to parse arguments and call methods for open/close
     * @param directionMethod
     * @param generalMethod
     * @returns {Function}
     */
    function openableActionFactory(directionMethod, generalMethod) {
        return function (arg1, arg2) {
            var adjacentOpenables = this.map.getAdjacentOpenables(this.location);
            var direction = this.getArgumentIsDirection(arg1);
            var object = this.getArgumentIsOpenable(arg1);

            if (!direction) {
                direction = this.getArgumentIsDirection(arg2);
            }
            if (!object) {
                object = this.getArgumentIsOpenable(arg2);
            }

            if (direction) {
                return this[directionMethod](object, direction);
            } else {
                return this[generalMethod](object, direction, adjacentOpenables);
            }
        };
    }

    return Player;
});
