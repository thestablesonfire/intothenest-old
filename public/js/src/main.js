requirejs.config({
    baseUrl: 'public/js/src/',
    paths: {
        BaseCommandDelegator: 'InputHandler/Delegators/BaseCommandDelegator',
        ConsoleCommands: 'InputHandler/Commands/consoleCommands',
        ConsoleCommandDelegator: 'InputHandler/Delegators/ConsoleCommandDelegator',
        Chest: 'Dungeon/Map/Tile/Openable/Chest/Chest',
        Directions: 'InputHandler/Commands/directions',
        Door: 'Dungeon/Map/Tile/Openable/Door/Door',
        GameDungeon: 'Dungeon/GameDungeon',
        Feature: 'Dungeon/Map/Feature/Feature',
        Hallway: 'Dungeon/Map/Feature/Hallway/Hallway',
        InputHandler: 'InputHandler/InputHandler',
        GameMap: 'Dungeon/Map/GameMap',
        MapFactory: 'Dungeon/Map/MapFactory',
        Openable: 'Dungeon/Map/Tile/Openable/Openable',
        Openables: 'InputHandler/Commands/openables',
        Pathfinder: 'Dungeon/Map/Pathfinder/Pathfinder',
        Player: 'Player/Player',
        PlayerCommands: 'InputHandler/Commands/playerCommands',
        PlayerCommandDelegator: 'InputHandler/Delegators/PlayerCommandDelegator',
        ResponseFactory: 'InputHandler/ResponseFactory',
        Room: 'Dungeon/Map/Feature/Room/Room',
        Staircase: 'Dungeon/Map/Tile/Staircase/Staircase',
        Tests: 'Tests/',
        Tile: 'Dungeon/Map/Tile/Tile',
        TileFactory: 'Dungeon/Map/Tile/TileFactory',
        TileType: 'Dungeon/Map/Tile/TileType',
        TileTypes: 'Dungeon/Map/Tile/tileTypes',
        Utilities: 'Utilities/Utilities',
        VisitedMap: 'Dungeon/Map/Pathfinder/VisitedMap/VisitedMap'
    }
});

var $input;
var $output;
var $mapDisplay;

requirejs(['IntoTheNest'], function(IntoTheNest) {
    $input = $("#command");
    $output = $("#response");
    $mapDisplay = $("#map_display");

    var ITN = new IntoTheNest($input, $output, $mapDisplay);
    registerEventListeners(ITN);
    ITN.init();
});

function registerEventListeners (ITN) {
    $input.keypress(function (e) {
        if (e.keyCode === 13) {
            var content = $(this).val();
            var response = ITN.input();
        }
    });
}
