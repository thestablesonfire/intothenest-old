define(['GameMap', 'Player', 'TileFactory'], function (GameMap, Player, TileFactory) {
    describe('Player', function () {
        var player = new Player();
        sinon.spy(player, 'revealTiles');
        sinon.stub(player, 'move', function () {return true;});

        var trueFunc = sinon.stub();
        trueFunc.returns(true);

        var tileFactory = new TileFactory();
        var blankTile1 = sinon.stub(tileFactory.getBlankTile(0,0));
        var blankTile2 = sinon.stub(tileFactory.getBlankTile(0,1));
        var blankTile3 = sinon.stub(tileFactory.getBlankTile(1,0));
        var blankTile4 = sinon.stub(tileFactory.getBlankTile(1,1));

        var tiles = [
            [blankTile1, blankTile2],
            [blankTile3, blankTile4]
        ];
        var map = new GameMap(2, 2, tiles,[],[]);
        
        it('should create well-formed player', function () {
            unitjs.assert(!player.getResultMessage());
            unitjs.assert(player.map === null);
            unitjs.assert(player.dungeonContext === null);
            unitjs.assert(player.moveDownStairs === null);
            unitjs.assert(player.moveUpStairs === null);
        });

        it('should set location', function () {
            player.setMap(map);
            unitjs.assert(player.map === map);
            player.setLocation({x: 0, y: 0});
            unitjs.assert(player.location.x === 0 && player.location.y === 0);
            unitjs.assert(player.revealTiles.calledOnce);
            map.setPlayer(player);
        });

        it('should set dungeon context', function () {
            var ctx = {};
            player.setDungeonContext(ctx);
            unitjs.assert(player.dungeonContext === ctx);
        });

        it('should set stair callbacks', function () {
            var upStairCallback = function () {
                return 'up';
            };

            var downStairCallback = function () {
                return 'down';
            };
            player.setUpStairCallback(upStairCallback);
            player.setDownStairCallback(downStairCallback);
            unitjs.assert(player.moveUpStairs === upStairCallback);
            unitjs.assert(player.moveDownStairs === downStairCallback);
            unitjs.assert(player.moveUpStairs() === 'up');
            unitjs.assert(player.moveDownStairs() === 'down');
        });

        it('should move as expected', function () {
            unitjs.assert(player.moveSouth());
            unitjs.assert(player.move.calledOnce);
            unitjs.assert(player.moveNorth());
            unitjs.assert(player.move.calledTwice);
            unitjs.assert(player.moveEast());
            unitjs.assert(player.move.calledThrice);
            unitjs.assert(player.moveWest());
            //unitjs.assert(player.move.calledOnce);
        });
    });
});