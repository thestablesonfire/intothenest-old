requirejs.config({
    baseUrl: '../src/',
    paths: {
        BaseCommandDelegator: 'InputHandler/Delegators/BaseCommandDelegator',
        ConsoleCommands: 'InputHandler/Commands/consoleCommands',
        ConsoleCommandDelegator: 'InputHandler/Delegators/ConsoleCommandDelegator',
        Chest: 'Dungeon/Map/Tile/Openable/Chest/Chest',
        Directions: 'InputHandler/Commands/directions',
        Door: 'Dungeon/Map/Tile/Openable/Door/Door',
        GameDungeon: 'Dungeon/GameDungeon',
        Feature: 'Dungeon/Map/Feature/Feature',
        Hallway: 'Dungeon/Map/Feature/Hallway/Hallway',
        InputHandler: 'InputHandler/InputHandler',
        GameMap: 'Dungeon/Map/GameMap',
        MapFactory: 'Dungeon/Map/MapFactory',
        Openable: 'Dungeon/Map/Tile/Openable/Openable',
        Openables: 'InputHandler/Commands/openables',
        Pathfinder: 'Dungeon/Map/Pathfinder/Pathfinder',
        Player: 'Player/Player',
        PlayerCommands: 'InputHandler/Commands/playerCommands',
        PlayerCommandDelegator: 'InputHandler/Delegators/PlayerCommandDelegator',
        ResponseFactory: 'InputHandler/ResponseFactory',
        Room: 'Dungeon/Map/Feature/Room/Room',
        Staircase: 'Dungeon/Map/Tile/Staircase/Staircase',
        Tile: 'Dungeon/Map/Tile/Tile',
        TileFactory: 'Dungeon/Map/Tile/TileFactory',
        TileType: 'Dungeon/Map/Tile/TileType',
        TileTypes: 'Dungeon/Map/Tile/tileTypes',
        Utilities: 'Utilities/Utilities',
        VisitedMap: 'Dungeon/Map/Pathfinder/VisitedMap/VisitedMap'
    }
});
require([
    'Dungeon/GameDungeon.spec.js',
    'Dungeon/Map/GameMap.spec.js',
    'Dungeon/Map/Feature/Room/Room.spec.js',
    'Dungeon/Map/Pathfinder/Pathfinder.spec.js',
    'Dungeon/Map/Tile/Tile.spec.js',
    'Dungeon/Map/Tile/TileFactory.spec.js',
    'Dungeon/Map/Tile/Openable/Chest/Chest.spec.js',
    'Dungeon/Map/Tile/Openable/Door/Door.spec.js',
    'Player/Player.spec.js',
    'Utilities/Utilities.spec.js'
    ], function() {
    if (typeof initMochaPhantomJS === 'function') {
        initMochaPhantomJS()
    } else {
        mocha.run();
    }
});
