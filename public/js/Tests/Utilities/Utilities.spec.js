define(['Utilities'], function (Utilities) {
    describe('Utilities', function () {
        it('should generate random numbers in the ranges set', function () {
            var min = 50;
            var max = 100;
            var num = Utilities.generateRandomNumber(min, max);
            unitjs.assert(num >= min && num <= max);
            unitjs.assert(num >= min && num <= max);
            unitjs.assert(num >= min && num <= max);
            unitjs.assert(num >= min && num <= max);
            unitjs.assert(num >= min && num <= max);
            unitjs.assert(num >= min && num <= max);
            unitjs.assert(num >= min && num <= max);
            unitjs.assert(num >= min && num <= max);
        });

        it('should determine if a string is in an array', function () {
            var testObj = {
                a: ['test1', 'test2', 'test3'],
                b: ['b1', 'b1', 'b1'],
                c: ['c1'],
                blank: ['']
            };
            unitjs.assert(Utilities.stringIsInObject('test2', testObj));
            unitjs.assert(Utilities.stringIsInObject('test1', testObj));
            unitjs.assert(Utilities.stringIsInObject('test3', testObj));
            unitjs.assert(Utilities.stringIsInObject('b1', testObj));
            unitjs.assert(Utilities.stringIsInObject('c1', testObj));
            unitjs.assert(Utilities.stringIsInObject('', testObj));

            unitjs.assert(!Utilities.stringIsInObject('test4', testObj));
            unitjs.assert(!Utilities.stringIsInObject('test12', testObj));
            unitjs.assert(!Utilities.stringIsInObject('1test1', testObj));
            unitjs.assert(!Utilities.stringIsInObject('&nbsp;', testObj));
        });

        it('should test with probability', function () {
            unitjs.assert(Utilities.testWithProbability(1));
            unitjs.assert(!Utilities.testWithProbability(0));
        });

        it('should get a random entry from an array', function () {
            var arr1 = ['sdf', 'test2', 'foo', 'bar', 'baz'];
            var entry = Utilities.getRandomEntryFromArray(arr1);
            unitjs.assert(~!!arr1.indexOf(entry));
        });
    });
});