define(['Room', 'Tile', 'TileTypes'], function (Room, Tile, TileTypes) {
    describe('Room', function () {
        var roomLocation = {x: 1, y: 1};
        var room = new Room(roomLocation);

        it('should generate a well-formed room', function () {
            var minHeight = 6;
            var minWidth = 6;
            var maxHeight = 10;
            var maxWidth = 10;
            var minDoors = 1;
            var maxDoors = 3;

            // Dimensions
            unitjs.assert(room.width >= minWidth && room.width <= maxWidth);
            unitjs.assert(room.height >= minHeight && room.height <= maxHeight);
            // Doors
            unitjs.assert(room.doors.length >= minDoors && room.doors.length <= maxDoors);
            // Stairs
            unitjs.assert(!room.stairs);
        });

        it('should globalize/de-globalize locations correctly', function () {
            var globalLocation = room.globalizeLocation(2, 1);
            unitjs.assert(globalLocation.y === roomLocation.y + 2);
            unitjs.assert(globalLocation.x === roomLocation.x + 1);
            var localLocation = room.deGlobalizeLocation(globalLocation);
            unitjs.assert(localLocation.y === 2);
            unitjs.assert(localLocation.x === 1);
        });

        it('should correctly identify outer-perimeter and non-outer-perimeter locations', function () {
            // Pass
            unitjs.assert(room.isOuterPerimeter(0,100));
            unitjs.assert(room.isOuterPerimeter(100,0));
            unitjs.assert(room.isOuterPerimeter(room.height-1,0));
            unitjs.assert(room.isOuterPerimeter(0,room.width-1));
            // Fail
            unitjs.assert(!room.isOuterPerimeter(1,100));
            unitjs.assert(!room.isOuterPerimeter(100,1));
        });

        it('should correctly identify inner-perimeter and non-inner-perimeter locations', function () {
            // Pass
            unitjs.assert(room.isInnerPerimeter(1, room.width-2));
            unitjs.assert(room.isInnerPerimeter(1,1));
            unitjs.assert(room.isInnerPerimeter(room.height-2,1));
            unitjs.assert(room.isInnerPerimeter(room.height-2,room.width-2));
            // Fail
            unitjs.assert(!room.isInnerPerimeter(0, room.width-2));
            unitjs.assert(!room.isInnerPerimeter(1,0));
            unitjs.assert(!room.isInnerPerimeter(room.height-1,1));
            unitjs.assert(!room.isInnerPerimeter(room.height-2,room.width-1));
        });

        it('should return all wall tiles', function () {
            var wallTiles = room.gatherWallTiles();
            var wallTileCount = ((((room.width-2) * 2) + ((room.height-2) * 2)) - 4);
            unitjs.assert(wallTiles.length === wallTileCount);

            var doorCount = 0;
            var wallCount = 0;
            for (var i = 0; i < wallTiles.length; i++)  {
                if (wallTiles[i].getType() === TileTypes.Wall) {
                    wallCount++;
                } else if (wallTiles[i].getType() === TileTypes.DoorClosed) {
                    doorCount++;
                }
            }

            unitjs.assert(doorCount === room.doors.length);
            unitjs.assert(wallCount === wallTileCount - doorCount);
        });

        it('should correctly identify corner tiles', function () {
            // Pass
            var tile = sinon.stub(new Tile(TileTypes.Wall, 1, 1));
            unitjs.assert(room.isCornerTile(tile));
            tile.location.x = room.width-2;
            unitjs.assert(room.isCornerTile(tile));
            tile.location.y = room.height - 2;
            unitjs.assert(room.isCornerTile(tile));
            room.location.x = 1;
            unitjs.assert(room.isCornerTile(tile));

            // Fail
            tile.location.x = 0;
            unitjs.assert(!room.isCornerTile(tile));
            tile.location.x = 1;
            tile.location.y = 0;
            unitjs.assert(!room.isCornerTile(tile));
            tile.location.y = room.height - 1;
            unitjs.assert(!room.isCornerTile(tile));
        });

        it('should correctly identify door-adjacent tiles', function () {
            // Have to de-globalize the room b/c the isDoorAdjacent method only works with local locations
            room.deGlobalizeRoom();

            var doors = room.doors;
            var adjacentCount = 0;

            for (var i = 0; i < 1; i++) {
                var thisDoorLocation = doors[i].location;
                var nLocation = {
                    x: thisDoorLocation.x,
                    y: thisDoorLocation.y
                };

                nLocation.y -= 1;
                testIsDoorAdjacent(nLocation);

                nLocation.y += 2;
                testIsDoorAdjacent(nLocation);

                nLocation.y -= 1;
                nLocation.x += 1;
                testIsDoorAdjacent(nLocation);

                nLocation.x -= 2;
                testIsDoorAdjacent(nLocation);

                nLocation.x += 1;
                testIsDoorAdjacent(nLocation);
            }

            unitjs.assert(adjacentCount === 4);
            room.globalizeRoom();

            function testIsDoorAdjacent(location) {
                if (room.tileExistsAtLocation(location)) {
                    var testTile = room.tiles[location.y][location.x];
                    var result = room.isDoorAdjacent(testTile);

                    if (result) {
                        adjacentCount++;
                    }
                }
            }
        });

        it('should replace a global tile correctly', function () {
            var xCoord = 3;
            var yCoord = 3;
            var newType = TileTypes.StairsUp;
            var newTile = sinon.stub(new Tile(newType, yCoord, xCoord));
            var oldTile = room.tiles[yCoord][xCoord];

            newTile.location = room.globalizeLocation(yCoord, xCoord);
            room.replaceRoomTileGlobal(oldTile, newTile);
            unitjs.assert(room.tiles[yCoord][xCoord] === newTile);
        });

        it('should return all of the correct floor tiles', function () {
            var floorTiles = room.getFloorTiles();
            var maxExpectedTiles = (room.height - 4) * (room.width - 4);
            var minExpectedTiles = maxExpectedTiles - 2;
            unitjs.assert(floorTiles.length >= minExpectedTiles && floorTiles.length <= maxExpectedTiles);

            for (var i = 0; i < floorTiles.length; i++) {
                unitjs.assert(floorTiles[i].getType() === TileTypes.Floor);
            }
        });

        it('should place a chest in the room', function () {
            var numChests = room.chests.length;
            room.placeChests(1);
            unitjs.assert(room.chests.length === numChests+1);
        });
    });
});
