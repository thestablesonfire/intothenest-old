define(['Chest'], function (Chest) {
    describe('Chest', function () {
        var chest = new Chest(0, 0);
        it('should be well-formed', function () {
            unitjs.assert(chest.className === 'Chest');
            unitjs.assert(!chest.inventory.length);
            unitjs.assert(!chest.getResultMessage());
        });

        it('should open', function () {
            unitjs.assert(chest.open());
            unitjs.assert(chest.getResultMessage());
            unitjs.assert(!chest.getResultMessage());
        });

        it('should close', function () {
            unitjs.assert(chest.close());
            unitjs.assert(chest.getResultMessage());
            unitjs.assert(!chest.getResultMessage());
        });
    });
});