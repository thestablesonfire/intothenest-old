define(['Door'], function (Door) {
    describe('Door', function () {
        var door = new Door(0, 0);
        it('should be well-formed', function () {
            unitjs.assert(door.className === 'Door');
            unitjs.assert(!door.getResultMessage());
        });

        it('should open', function () {
            unitjs.assert(door.open());
            unitjs.assert(door.getResultMessage());
            unitjs.assert(!door.getResultMessage());
        });

        it('should close', function () {
            unitjs.assert(door.close());
            unitjs.assert(door.getResultMessage());
            unitjs.assert(!door.getResultMessage());
        });
    });
});
