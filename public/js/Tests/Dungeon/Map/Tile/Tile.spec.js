define(['Tile'], function (Tile) {
    describe('Tile', function () {
        var type = {
            symbol: 'X',
            passable: true,
            description: 'a description',
            classes: ['c1', 'c2']
        };
        var tile = new Tile(type, 0, 0);

        it('should return correct description', function () {
            unitjs.assert(tile.getDescription() === type.description);
        });

        it('should return correct symbol HTML', function () {
            var symbol = '<span class="c1 c2 fogged">' + type.symbol + '</span>';
            unitjs.assert(tile.getSymbol() === symbol);
        });

        it('should return correct full type', function () {
            unitjs.assert(tile.getType() === type);
        });

        it('should return correct adjacent locations', function () {
            var adjacent = tile.getAdjacentTiles();
            var north = {x: tile.location.x, y: tile.location.y-1};
            var east = {x: tile.location.x+1, y: tile.location.y};
            var south = {x: tile.location.x, y: tile.location.y+1};
            var west = {x: tile.location.x-1, y: tile.location.y};

            unitjs.assert(adjacent[0].x === north.x && adjacent[0].y === north.y);
            unitjs.assert(adjacent[1].x === east.x && adjacent[1].y === east.y);
            unitjs.assert(adjacent[2].x === south.x && adjacent[2].y === south.y);
            unitjs.assert(adjacent[3].x === west.x && adjacent[3].y === west.y);
        });

        it('should return correct surrounding locations', function () {
            var surrounding = tile.getSurroundingTiles();
            var north = {x: tile.location.x, y: tile.location.y-1};
            var east = {x: tile.location.x+1, y: tile.location.y};
            var south = {x: tile.location.x, y: tile.location.y+1};
            var west = {x: tile.location.x-1, y: tile.location.y};
            var northeast = {x: tile.location.x+1, y: tile.location.y-1};
            var southeast = {x: tile.location.x+1, y: tile.location.y+1};
            var southwest = {x: tile.location.x-1, y: tile.location.y+1};
            var northwest = {x: tile.location.x-1, y: tile.location.y-1};

            unitjs.assert(surrounding[0].x === north.x && surrounding[0].y === north.y);
            unitjs.assert(surrounding[1].x === east.x && surrounding[1].y === east.y);
            unitjs.assert(surrounding[2].x === south.x && surrounding[2].y === south.y);
            unitjs.assert(surrounding[3].x === west.x && surrounding[3].y === west.y);
            unitjs.assert(surrounding[4].x === northeast.x && surrounding[4].y === northeast.y);
            unitjs.assert(surrounding[5].x === southeast.x && surrounding[5].y === southeast.y);
            unitjs.assert(surrounding[6].x === southwest.x && surrounding[6].y === southwest.y);
            unitjs.assert(surrounding[7].x === northwest.x && surrounding[7].y === northwest.y);
        });

        it('should set a new type', function () {
            var newType = {
                symbol: 'Y',
                passable: false,
                description: 'a new description',
                classes: ['c3', 'c4']
            };
            tile.setType(newType);
            unitjs.assert(tile.getType() === newType);
        });

        it('should return correct passable boolean', function () {
            unitjs.assert(!tile.isPassable());
        });

        it('should verify locations correctly', function () {
            var newTile = new Tile(type, 0, 0);
            var notSameTile = new Tile(type, 1, 0);
            unitjs.assert(tile.hasSameLocation(newTile));
            unitjs.assert(!tile.hasSameLocation(notSameTile));
        });
    });
});