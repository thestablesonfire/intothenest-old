define(['TileFactory', 'TileTypes'], function (TileFactory, TileTypes) {
    describe('TileFactory', function () {
        var tileFactory = new TileFactory();

        it('should return a well-formed blank tile', function () {
            var type = TileTypes.Blank;
            var blankTile = tileFactory.getBlankTile(0, 0);
            unitjs.assert(blankTile.type === type);
            checkTile(blankTile, type);
        });

        it('should return a well-formed floor tile', function () {
            var type = TileTypes.Floor;
            var floorTile = tileFactory.getFloorTile(0, 0);
            unitjs.assert(floorTile.type === type);
            checkTile(floorTile, type);
        });

        it('should return a well-formed hallway tile', function () {
            var type = TileTypes.Hallway;
            var hallTile = tileFactory.getHallwayTile(0, 0);
            unitjs.assert(hallTile.type === type);
            checkTile(hallTile, type);
        });

        it('should return a well-formed wall tile', function () {
            var type = TileTypes.Wall;
            var wallTile = tileFactory.getWallTile(0, 0);
            unitjs.assert(wallTile.type === type);
            checkTile(wallTile, type);
        });

        it('should return a well-formed up stair tile', function () {
            var type = TileTypes.StairsUp;
            var upStairTile = tileFactory.getUpStair(0, 0);
            unitjs.assert(upStairTile.type === type);
            checkTile(upStairTile, type);
        });

        it('should return a well-formed down stair tile', function () {
            var type = TileTypes.StairsDown;
            var downStairTile = tileFactory.getDownStair(0, 0);
            unitjs.assert(downStairTile.type === type);
            checkTile(downStairTile, type);
        });

        it('should return a well-formed door tile', function () {
            var type = TileTypes.DoorClosed;
            var doorTile = tileFactory.getDoorTile(0, 0);
            unitjs.assert(doorTile.type === type);
            checkTile(doorTile, type);
        });

        it('should return a well-formed chest tile', function () {
            var type = TileTypes.ChestClosed;
            var chestTile = tileFactory.getChestTile(0, 0);
            unitjs.assert(chestTile.type === type);
            checkTile(chestTile, type);
        });

        function checkTile(tile, type) {
            unitjs.assert(tile.location.x === 0 && tile.location.y === 0);
            unitjs.assert(!tile.isVisible);
            unitjs.assert(tile.isFogged);

            unitjs.assert(tile.getDescription() === type.description);

        }
    });
});