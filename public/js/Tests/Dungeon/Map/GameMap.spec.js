define(['MapFactory', 'Player', 'TileTypes'], function (MapFactory, Player, TileTypes) {
    describe('Game Map and Factory', function () {
        var height = 20;
        var width = 20;
        var numRooms = 2;
        var maxDoors = 3;
        var minDoors = 1;
        var mapFactory = new MapFactory(height, width);
        sinon.spy(mapFactory, 'getBlankMap');
        sinon.spy(mapFactory, 'generateRooms');
        sinon.spy(mapFactory, 'placeStairs');
        sinon.spy(mapFactory, 'generateHallways');
        sinon.spy(mapFactory, 'updateBlankTiles');
        sinon.spy(mapFactory, 'createMap');
        sinon.spy(mapFactory, 'setBookKeepingValues');
        sinon.spy(mapFactory, 'reset');

        var map = mapFactory.getRandomMap(numRooms);

        var player = sinon.stub(new Player());
        map.setPlayer(player);

        var upStair;
        var downStair;

        it('should be a well-formed map', function () {
            unitjs.assert(mapFactory.getBlankMap.calledOnce);
            unitjs.assert(mapFactory.generateRooms.calledOnce);
            unitjs.assert(mapFactory.placeStairs.calledOnce);
            unitjs.assert(mapFactory.generateHallways.calledOnce);
            unitjs.assert(mapFactory.updateBlankTiles.calledOnce);
            unitjs.assert(mapFactory.createMap.calledOnce);
            unitjs.assert(mapFactory.setBookKeepingValues.calledOnce);
            unitjs.assert(mapFactory.reset.calledOnce);

            // Check dimensions
            unitjs.assert(map.height = height);
            unitjs.assert(map.width = width);

            // Check tiles
            unitjs.assert(map.tiles.length === height);
            unitjs.assert(map.tiles[0].length === width);

            // Player is set
            unitjs.assert(map.player === player);

            // Should have 2 rooms
            unitjs.assert(map.rooms.length === numRooms);

            // Should have the correct # of hallways
            unitjs.assert(map.hallwaysGenerated >= (numRooms & minDoors) && map.hallwaysGenerated <= (numRooms * maxDoors));
        });

        it('should have created one up stair and one down stair', function () {
            unitjs.assert(map.stairs.length === 2);
            upStair = map.stairs[0];
            downStair = map.stairs[1];
            unitjs.assert(upStair.getType() === TileTypes.StairsUp);
            unitjs.assert(downStair.getType() === TileTypes.StairsDown);
            unitjs.assert(map.getUpStair() === upStair);
            unitjs.assert(map.getDownStair() === downStair);
            unitjs.assert(map.tileIsUpStair(upStair.location));
            unitjs.assert(map.tileIsDownStair(downStair.location));
            unitjs.assert(map.tileIsPassable(upStair.location));
            unitjs.assert(!map.tileIsPassable({x: 0, y: 0}));
        });

        it('should return the correct tiles', function () {
            // Get tiles around central tile
            var northTile = map.getTileFromDirection(upStair.location, 'North');
            var eastTile = map.getTileFromDirection(upStair.location, 'East');
            var southTile = map.getTileFromDirection(upStair.location, 'South');
            var westTile = map.getTileFromDirection(upStair.location, 'West');
            unitjs.assert(northTile && eastTile && southTile && westTile);

            // Check adjacent tiles
            var tiles = map.getAdjacentTiles(upStair.location);
            unitjs.assert(tiles.length === 4);
            unitjs.assert(tiles[0] === northTile);
            unitjs.assert(tiles[1] === eastTile);
            unitjs.assert(tiles[2] === southTile);
            unitjs.assert(tiles[3] === westTile);
        });

        it('should return print output html', function () {
            // Print output
            var printHTML = map.print();
            unitjs.assert(printHTML);
        });

        it('should correctly fog tile', function () {
            // Test fogger
            unitjs.assert(upStair.isFogged);
            upStair.isFogged = false;
            unitjs.assert(!upStair.isFogged);
            map.setFog();
            unitjs.assert(upStair.isFogged);
        });
    });
});
