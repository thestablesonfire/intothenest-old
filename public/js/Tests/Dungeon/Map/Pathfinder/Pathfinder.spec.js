define(['MapFactory', 'Pathfinder', 'Player', 'TileTypes'], function (MapFactory, Pathfinder, Player, TileTypes) {
    describe('Pathfinder', function () {
        var blankMap = new MapFactory(20, 20).getBlankMap();
        var player = sinon.stub(new Player());

        it('should find a simple path', function () {
            var startTile = blankMap.tiles[0][0];
            var endTile = blankMap.tiles[15][15];
            var pathfinder = new Pathfinder(startTile, endTile, blankMap.tiles);
            var path = pathfinder.findPath();
            unitjs.assert(path.length === 15*2-1);
        });

        it('should find an opposite simple path', function () {
            var startTile = blankMap.tiles[15][15];
            var endTile = blankMap.tiles[0][0];
            var pathfinder = new Pathfinder(startTile, endTile, blankMap.tiles);
            var path = pathfinder.findPath();
            unitjs.assert(path);
            unitjs.assert(path.length === 15*2-1);
        });

        it('should find a path between wall', function () {
            blankMap = new MapFactory(5, 5).getBlankMap();
            blankMap.tiles[2][0].setType(TileTypes.Wall);
            blankMap.tiles[2][1].setType(TileTypes.Wall);
            blankMap.tiles[2][3].setType(TileTypes.Wall);
            blankMap.tiles[2][4].setType(TileTypes.Wall);

            var startTile = blankMap.tiles[0][0];
            var endTile = blankMap.tiles[4][4];
            var pathfinder = new Pathfinder(startTile, endTile, blankMap.tiles);
            var path = pathfinder.findPath();
            unitjs.assert(path);
            unitjs.assert(path.length === 4*2-1);
        });

        it('should fail to find a path', function () {
            blankMap.tiles[2][2].setType(TileTypes.Wall);
            var startTile = blankMap.tiles[0][0];
            var endTile = blankMap.tiles[4][4];
            var pathfinder = new Pathfinder(startTile, endTile, blankMap.tiles);
            var path = pathfinder.findPath();
            unitjs.assert(!path);
        });
    });
});
