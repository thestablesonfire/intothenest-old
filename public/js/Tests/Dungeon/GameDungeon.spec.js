define(['GameDungeon', 'MapFactory', 'Player'], function (GameDungeon, MapFactory, Player) {
    describe('Game Dungeon', function () {
        var player = sinon.stub(new Player());
        var gameDungeon = new GameDungeon(player);
        sinon.spy(gameDungeon, 'updatePlayerLocation');

        it('should setup properly', function () {
            gameDungeon.init();
            unitjs.assert(gameDungeon.maps.length === 1);
            unitjs.assert(!gameDungeon.curMapIndex);
            unitjs.assert(gameDungeon.updatePlayerLocation.calledOnce);
            unitjs.assert(player.setMap.calledOnce);
            unitjs.assert(player.setDungeonContext.calledOnce);
            unitjs.assert(player.setDownStairCallback.calledOnce);
            unitjs.assert(player.setUpStairCallback.calledOnce);
            unitjs.assert(gameDungeon.getCurrentMap().player === player);
        });

        it('should add a new map to maps array', function () {
            gameDungeon.playerMovedDown();
            unitjs.assert(gameDungeon.maps.length === 2);
            unitjs.assert(gameDungeon.curMapIndex === 1);
            unitjs.assert(gameDungeon.updatePlayerLocation.calledTwice);
        });

        it('should not add a new map, but update the curMapIndex', function () {
            gameDungeon.playerMovedUp();
            unitjs.assert(gameDungeon.maps.length === 2);
            unitjs.assert(gameDungeon.curMapIndex === 0);
        });

        it('should not allow the player to move up anymore', function () {
            gameDungeon.playerMovedUp();
            unitjs.assert(gameDungeon.maps.length === 2);
            unitjs.assert(gameDungeon.curMapIndex === 0);
        });

    });
});
